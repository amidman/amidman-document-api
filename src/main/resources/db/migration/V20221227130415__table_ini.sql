
create table if not exists Users(
    id bigserial primary key,
    login varchar(32) not null unique,
    password varchar(128) not null,
    name varchar(32) not null,
    surname varchar(32) not null,
    fatherName varchar(32) default '',
    role varchar(32) not null
);


create table if not exists Departments(
    id bigserial primary key,
    departmentName varchar(128) unique not null,
    departmentShortName varchar(8) default 'NN'
);

create table if not exists Groups(
    id bigserial primary key,
    groupName varchar(32) not null unique,
    status varchar(32) not null,
    yearCount int not null,
    maxYearCount int not null,
    departmentId bigint references Departments(id)
);

create table if not exists StudentDocuments(
    id bigserial primary key,
    docNumber varchar(64) unique not null,
    orderNumber varchar(64) not null,
    orderDate date not null,
    studyStartDate date not null
);

create table if not exists Students(
    id bigserial primary key,
    userId bigint references Users(id) on delete cascade unique,
    groupId bigint references Groups(id) on delete cascade,
    studentDocumentId bigint references StudentDocuments(id) on delete cascade
);

create table if not exists StudentRequests(
    id bigserial primary key,
    studentId bigint references Students(id) on delete cascade,
    status varchar(16) not null,
    type varchar(16) not null,
    departmentId bigint references Departments(id) on delete cascade,
    countValue int not null,
    dateOfRequest date not null
);
