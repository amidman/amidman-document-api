alter table users alter column id set data type int;
alter table departments alter column id set data type int;
alter table groups alter column id set data type int;
alter table studentdocuments alter column id set data type int;
alter table studentrequests alter column id set data type int;
alter table students alter column id set data type int;
alter table students alter column userid set data type int;
alter table students alter column groupid set data type int;
alter table students alter column studentdocumentid set data type int;
alter table studentrequests alter column studentid set data type int;
alter table studentrequests alter column departmentid set data type int;
alter table groups alter column departmentid set data type int;

