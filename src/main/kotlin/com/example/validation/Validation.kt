package com.example.validation

import io.ktor.server.application.*
import io.ktor.server.plugins.requestvalidation.*

fun Application.configureValidation() {
    install(RequestValidation){
        departmentValidation()
        groupValidation()
        studentValidation()
        studentRequestValidation()
        studentDocumentValidation()
        userValidation()
    }
}