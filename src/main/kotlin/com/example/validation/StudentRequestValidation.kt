package com.example.validation

import com.example.domain.model.student.validate
import com.example.domain.model.student_request.RequestModel
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.studentRequestValidation(){
    validate<RequestModel> { requestModel ->
        requestModel.student.validate()
        ValidationResult.Valid
    }
}