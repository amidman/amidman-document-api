package com.example.validation

import com.example.domain.model.department.DepartmentDTO
import com.example.domain.model.department.DepartmentModel
import com.example.domain.model.department.validate
import com.example.domain.model.group.validate
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.departmentValidation(){
    validate<DepartmentDTO>{ departmentDTO ->
        departmentDTO.validate()
        ValidationResult.Valid
    }
    validate<DepartmentModel>{departmentModel ->
        departmentModel.validate()
        ValidationResult.Valid
    }
}