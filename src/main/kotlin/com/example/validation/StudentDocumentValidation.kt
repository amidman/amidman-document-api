package com.example.validation

import com.example.domain.model.student_document.StudentDocumentDTO
import com.example.domain.model.student_document.validate
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.studentDocumentValidation(){
    validate<StudentDocumentDTO>{ studentDocumentDTO ->
        studentDocumentDTO.validate()
        ValidationResult.Valid
    }
}