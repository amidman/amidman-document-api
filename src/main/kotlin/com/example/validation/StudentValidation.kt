package com.example.validation

import com.example.domain.model.student.StudentCreateModel
import com.example.domain.model.student.StudentDTO
import com.example.domain.model.student.StudentModel
import com.example.domain.model.student.validate
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.studentValidation(){
    validate<StudentDTO>{ studentDTO ->
        studentDTO.validate()
        ValidationResult.Valid
    }
    validate<StudentModel>{ studentModel ->
        studentModel.validate()
        ValidationResult.Valid
    }
    validate<StudentCreateModel>{ studentCreateModel ->
        studentCreateModel.validate()
        ValidationResult.Valid
    }
}