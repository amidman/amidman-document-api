package com.example.validation

import com.example.domain.model.department.validate
import com.example.domain.model.group.GroupDTO
import com.example.domain.model.group.GroupModel
import com.example.domain.model.group.validate
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.groupValidation(){
    validate<GroupModel>{ groupModel ->
        groupModel.validate()
        ValidationResult.Valid
    }
    validate<GroupDTO>{ groupDTO ->
        groupDTO.validate()
        ValidationResult.Valid
    }

}