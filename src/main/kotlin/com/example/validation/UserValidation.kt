package com.example.validation

import com.example.domain.model.user.UserDTO
import com.example.domain.model.user.UserFIO
import com.example.domain.model.user.validate
import io.ktor.server.plugins.requestvalidation.*

fun RequestValidationConfig.userValidation(){
    validate<UserFIO>{ userFIO ->
        userFIO.validate()
        ValidationResult.Valid
    }
    validate<UserDTO>{ userDTO ->
        userDTO.validate()
        ValidationResult.Valid
    }
}