package com.example

import com.example.database.configureDataBase
import com.example.di.configureKoinDI
import io.ktor.server.application.*
import com.example.plugins.*
import com.example.routing.configureRouting
import com.example.security.configureSecurity
import com.example.serialization.configureSerialization
import com.example.validation.configureValidation

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // application.conf references the main function. This annotation prevents the IDE from marking it as unused.
fun Application.module() {
    configureKoinDI()
    configureDataBase()
    configureSerialization()
    configureSecurity()
    configureSwaggerUI()
    configureStatusPages()
    configureValidation()
    configureRouting()
}
