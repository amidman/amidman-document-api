package com.example.errors

import com.example.errors.utils.ResponseException

interface EntityError {
    val NOT_FOUND: ResponseException
}