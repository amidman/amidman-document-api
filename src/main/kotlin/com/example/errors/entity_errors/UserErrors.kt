package com.example.errors.entity_errors

import com.example.errors.EntityError
import com.example.errors.utils.ErrorData
import com.example.errors.utils.ResponseException
import io.ktor.http.*

object UserErrors:EntityError {

    private const val not_found = "not found"
    private const val wrong_login = "wrong login"
    private const val wrong_role = "wrong role"
    private const val wrong_password = "wrong password"
    private const val email_unique = "email unique"
    private const val wrong_email = "wrong email"

    override val NOT_FOUND: ResponseException = ResponseException(
        ErrorData(
            code = not_found,
            description = "Пользователь не найден"
        ),
        HttpStatusCode.NotFound
    )

    val WRONG_LOGIN = ResponseException(
        ErrorData(
            code = wrong_login,
            description = "Пользователь с данным логином уже существует"
        ),
        HttpStatusCode.BadRequest
    )

    val WRONG_ROLE = ResponseException(
        ErrorData(
            code = wrong_role,
            description = "Указанной вами роли не существует"
        ),
        HttpStatusCode.BadRequest
    )

    val WRONG_PASSWORD = ResponseException(
        ErrorData(
            code = wrong_password,
            description = "Неверный пароль"
        ),
        HttpStatusCode.BadRequest
    )

    val EMAIL_UNIQUE = ResponseException(
        ErrorData(
            code = email_unique,
            description = "Пользователь с такой почтой уже зарегистрирован"
        ),
        HttpStatusCode.BadRequest
    )

    val WRONG_EMAIL = ResponseException(
        ErrorData(
            code = email_unique,
            description = "Неверный формат почты"
        ),
        HttpStatusCode.BadRequest
    )
}