package com.example.errors.entity_errors

import com.example.errors.EntityError
import com.example.errors.utils.ErrorData
import com.example.errors.utils.ResponseException
import io.ktor.http.*

object StudentErrors:EntityError {

    private const val not_found = "not found"

    override val NOT_FOUND: ResponseException
        get() = ResponseException(
            ErrorData(
                code = not_found,
                description = "Студент не найден"
            ),
            HttpStatusCode.NotFound
        )
}