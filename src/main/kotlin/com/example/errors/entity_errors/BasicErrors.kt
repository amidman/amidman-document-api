package com.example.errors.entity_errors

import com.example.errors.EntityError
import com.example.errors.utils.ErrorData
import com.example.errors.utils.ResponseException
import io.ktor.http.*

object BasicErrors:EntityError {
    private const val not_found_code = "not found"
    private const val empty_fields = "empty fields"
    private const val bad_request = "bad request"
    private const val wrong_date = "wrong date"
    private const val many_symbols = "many symbols"
    private const val database_error = "database error"

    override val NOT_FOUND = ResponseException(
        ErrorData(
            code = not_found_code,
            description = "По вашему запросу ничего не найдено"
        ),
        HttpStatusCode.NotFound
    )

    val EMPTY_FIELDS = ResponseException(
        ErrorData(
            code = empty_fields,
            description = "Пожалуйста заполните все поля"
        ),
        HttpStatusCode.BadRequest
    )

    val BAD_REQUEST = ResponseException(
        ErrorData(
            code = bad_request,
            description = "Проверьте правильность введённых данных и повторите попытку"
        ),
        HttpStatusCode.BadRequest
    )

    val DATABASE_ERROR = ResponseException(
        ErrorData(
            code = database_error,
            description = "Ошибка в базе данных, обратитесь к администратору"
        ),
        HttpStatusCode.BadRequest
    )

    val WRONG_DATE_FORMAT = ResponseException(
        ErrorData(
            code = wrong_date,
            description = "Неправильный формат даты"
        ),
        HttpStatusCode.BadRequest
    )

    fun MANY_SYMBOLS(fieldName:String,size:Int) = ResponseException(
        ErrorData(
            code = many_symbols,
            description = "Длина поля $fieldName ограничена $size символами"
        ),
        HttpStatusCode.BadRequest,
    )


}