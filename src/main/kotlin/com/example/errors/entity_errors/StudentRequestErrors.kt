package com.example.errors.entity_errors

import com.example.errors.EntityError
import com.example.errors.utils.ErrorData
import com.example.errors.utils.ResponseException
import io.ktor.http.*

object StudentRequestErrors:EntityError {

    private const val not_found = "not found"
    private const val send_later = "send later"
    private const val delete_accepted = "delete accepted"
    private const val already_accepted = "already accepted"

    override val NOT_FOUND: ResponseException
        get() = ResponseException(
            ErrorData(
                code = not_found,
                description = "Заявка не найдена"
            ),
            HttpStatusCode.NotFound
        )

    val SEND_LATER = ResponseException(
        ErrorData(
            code = send_later,
            description = "Заявку можно отправить раз в неделю, попробуйте позже",
        ),
        HttpStatusCode.BadRequest
    )

    val DELETE_ACCEPTED = ResponseException(
        ErrorData(
            code = delete_accepted,
            description = "Невозможно отменить подтвеждённый запрос"
        ),
        HttpStatusCode.BadRequest
    )

    val ALREADY_ACCEPTED = ResponseException(
        ErrorData(
            code = already_accepted,
            description = "Данная заявка уже подтверждена"
        ),
        HttpStatusCode.BadRequest
    )
}