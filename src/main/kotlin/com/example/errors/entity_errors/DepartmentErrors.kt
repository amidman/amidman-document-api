package com.example.errors.entity_errors

import com.example.errors.EntityError
import com.example.errors.utils.ErrorData
import com.example.errors.utils.ResponseException
import io.ktor.http.*

object DepartmentErrors:EntityError {

    private const val not_found = "not found"
    private const val wrong_name = "wrong name"

    override val NOT_FOUND: ResponseException
        get() = ResponseException(
            ErrorData(
                code = not_found,
                description = "Отделение не найдено"
            ),
            HttpStatusCode.NotFound
        )

    val WRONG_NAME = ResponseException(
        ErrorData(
            code = wrong_name,
            description = "Отделение с таким именем уже существует"
        ),
        HttpStatusCode.BadRequest
    )
}