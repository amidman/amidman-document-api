package com.example.errors.entity_errors

import com.example.errors.EntityError
import com.example.errors.utils.ErrorData
import com.example.errors.utils.ResponseException
import io.ktor.http.*

object GroupErrors:EntityError {

    private const val not_found = "not found"
    private const val wrong_name = "wrong name"
    private const val wrong_status = "wrong status"
    private const val wrong_year_count = "wrong year count"
    private const val wrong_max_year_count = "wrong max year count"
    override val NOT_FOUND: ResponseException
        get() = ResponseException(
            ErrorData(
                code = not_found,
                description = "Группа не найдена"
            ),
            HttpStatusCode.NotFound
        )

    val WRONG_NAME = ResponseException(
        ErrorData(
            code = wrong_name,
            description = "Группа с таким названием уже существует"
        ),
        HttpStatusCode.BadRequest
    )

    val WRONG_STATUS = ResponseException(
        ErrorData(
            code = wrong_status,
            description = "Неправильно указан статус группы, он должен быть равен BUDGET или NOBUDGET"
        ),
        HttpStatusCode.BadRequest
    )
    val WRONG_GROUP_YEAR_COUNT = ResponseException(
        ErrorData(
            code = wrong_year_count,
            description = "Значение курса группы должно находится в диапазоне от 1 до 4 и не может превышать максимальный срок обучения"
        ),
        HttpStatusCode.BadRequest
    )

    val WRONG_GROUP_MAX_YEAR_COUNT = ResponseException(
        ErrorData(
            code = wrong_max_year_count,
            description = "Максимальное время обучения может равняться 3 или 4 годам"
        )
    )

}