package com.example.errors.utils

import io.ktor.http.*

data class ResponseException(
    val errorData: ErrorData,
    val statusCode: HttpStatusCode = HttpStatusCode.NotFound
):Exception()