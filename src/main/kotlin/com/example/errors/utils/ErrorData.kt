package com.example.errors.utils

import kotlinx.serialization.Serializable

@Serializable
data class ErrorData(
    val code:String,
    val description:String
)