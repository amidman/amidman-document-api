package com.example.security.middleware

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTCreator
import com.auth0.jwt.algorithms.Algorithm
import com.example.domain.model.config.JWTConfig
import java.util.*


interface TokenProvider{

    fun createStudentToken(userId: Int,studentId: Int):String

    fun createAdminToken(userId: Int):String

    fun createSecretaryToken(userId:Int):String

}

class TokenProviderImpl(
    private val jwtConfig: JWTConfig,
):TokenProvider {

    private val studentTokenExpires = 3600000L * 24 * 30
    private val adminTokenExpires = 3600000L * 24 * 2

    private fun createBaseToken():JWTCreator.Builder{
        return JWT.create()
            .withAudience(jwtConfig.audience)
            .withIssuer(jwtConfig.issuer)
    }

    override fun createStudentToken(userId: Int,studentId:Int):String {
        return createBaseToken()
            .withClaim(Claims.ID, userId)
            .withClaim(Claims.ROLE, ROLES.STUDENT.name)
            .withClaim(Claims.STUDENT_ID,studentId)
            .withExpiresAt(Date(System.currentTimeMillis() + studentTokenExpires))
            .sign(Algorithm.HMAC256(jwtConfig.secret))
    }

    override fun createAdminToken(userId: Int,): String {
        return createBaseToken()
            .withClaim(Claims.ID, userId)
            .withClaim(Claims.ROLE, ROLES.ADMIN.name)
            .withExpiresAt(Date(System.currentTimeMillis() + adminTokenExpires))
            .sign(Algorithm.HMAC256(jwtConfig.secret))
    }

    override fun createSecretaryToken(userId: Int): String {
        return createBaseToken()
            .withClaim(Claims.ID, userId)
            .withClaim(Claims.ROLE, ROLES.SECRETARY.name)
            .withExpiresAt(Date(System.currentTimeMillis() + adminTokenExpires))
            .sign(Algorithm.HMAC256(jwtConfig.secret))
    }



}

fun main(){
    val jwtConfig = JWTConfig(
        secret = "b539f887d3ceb4df54644cddcf1fc0a99617a90b9ee204274593275df092e93cb539f887d3ceb4df54644cddcf1fc0a99617a90b9ee2",
        issuer = "https://jwt-provider-domain/",
        audience = "amidman-audience",
        realm = "amidman-realm",
    )
//    val jwtConfig = JWTConfig(
//        secret = "b539f887d3ceb4df54644cddcf1fc0a99617a90b9ee204274593275df092e93c",
//        issuer = "https://jwt-provider-domain/",
//        audience = "jwt-audience",
//        realm = "amidman",
//    )
    val provider = TokenProviderImpl(jwtConfig)
    println("admin")
    println(provider.createAdminToken(1))
    println("secretary")
    println(provider.createSecretaryToken(1))
}