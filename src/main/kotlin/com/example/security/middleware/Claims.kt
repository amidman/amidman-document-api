package com.example.security.middleware

object Claims {

    const val ID = "id"

    const val ROLE = "role"

    const val STUDENT_ID = "studentId"
}