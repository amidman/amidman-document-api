package com.example.security

import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTDecodeException
import io.ktor.server.application.*
import com.auth0.jwt.exceptions.TokenExpiredException
import com.example.domain.model.config.JWTConfig
import com.example.errors.middleware_errors.TokenErrors
import com.example.security.middleware.Claims
import com.example.security.middleware.ROLES
import org.koin.ktor.ext.inject





private fun JWTChallengeContext.jwtErrorHandler(verifier:JWTVerifier){
    val jwt = call.request.headers["Authorization"]?.replace("Bearer ","") ?: throw TokenErrors.INVALID_TOKEN
    try {
        verifier.verify(jwt)
    }catch (e:TokenExpiredException){
        throw TokenErrors.EXPIRED
    }catch (e:JWTDecodeException){
        throw TokenErrors.INVALID_TOKEN
    }
    throw TokenErrors.FORBIDDEN
}
private fun JWTAuthenticationProvider.Config.baseVerifier(config: JWTConfig){
    realm = config.realm
    val verifier = JWT
        .require(Algorithm.HMAC256(config.secret))
        .withAudience(config.audience)
        .withIssuer(config.issuer)
        .build()
    verifier(verifier)
    challenge {defaultSheme,realm ->
        jwtErrorHandler(verifier)
    }
}

private fun JWTCredential.baseValidate(role: ROLES?=null):Boolean
        =
    payload.getClaim(Claims.ID) != null &&
            payload.getClaim(Claims.ROLE) != null &&
            role?.let { payload.getClaim(Claims.ROLE).asString() == role.name} ?: true

private fun JWTCredential.studentValidate():Boolean = baseValidate(ROLES.STUDENT)
        && payload.getClaim(Claims.STUDENT_ID) != null


private fun JWTAuthenticationProvider.Config.roleValidate(role: ROLES? = null){
    when(role){
        null -> validate { if (it.baseValidate()) JWTPrincipal(it.payload) else null }
        ROLES.STUDENT -> validate { if (it.studentValidate()) JWTPrincipal(it.payload) else null }
        ROLES.ADMIN -> validate { if (it.baseValidate(ROLES.ADMIN)) JWTPrincipal(it.payload) else null }
        ROLES.SECRETARY -> validate { if (it.baseValidate(ROLES.SECRETARY)) JWTPrincipal(it.payload) else null }
    }
}

fun Application.configureSecurity() {

    val config: JWTConfig by inject()

    install(Authentication){
        jwt {
            baseVerifier(config)
            roleValidate()
        }
        ROLES.values().forEach { role->
            jwt(role.name) {
                baseVerifier(config)
                roleValidate(role)
            }
        }
    }
}




