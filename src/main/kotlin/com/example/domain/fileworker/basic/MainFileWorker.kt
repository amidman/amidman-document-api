package com.example.domain.fileworker.basic

import com.aspose.pdf.Document
import com.example.domain.fileworker.StudyDocumentFileWorker
import com.example.domain.model.student_request.StudentRequestType
import com.example.domain.model.student.StudentModel
import com.example.domain.model.student_request.StudentRequestDTO
import kotlinx.datetime.LocalDate

class MainFileWorker(
    private val studyDocumentFileWorker: StudyDocumentFileWorker
) {

    private val fileTypeMap = hashMapOf<StudentRequestType,FileWorker>(
        StudentRequestType.STUDYDOCUMENT to studyDocumentFileWorker
    )
    suspend fun handleRequestType(request: StudentRequestDTO, studentModel:StudentModel, currentDate:LocalDate):Document =
        fileTypeMap.getOrDefault(request.type,studyDocumentFileWorker)
            .generateFile(currentDate,request,studentModel)
}