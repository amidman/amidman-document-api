package com.example.domain.fileworker.basic

import com.aspose.pdf.Document
import com.aspose.pdf.FontRepository
import com.aspose.pdf.TextFragmentAbsorber
import com.example.domain.model.config.FileConfig
import com.example.domain.model.config.FontConfig
import com.example.domain.model.student.StudentModel
import com.example.domain.model.student_request.StudentRequestDTO
import kotlinx.datetime.LocalDate

abstract class FileWorker {

    protected abstract val fontConfig:FontConfig

    abstract suspend fun generateFile(currentDate: LocalDate, request: StudentRequestDTO, studentModel: StudentModel):Document

    fun replaceValuesInFile(filePath:String,replaceValues:HashMap<String,String>):Document{
        val document = Document(filePath)
        val tnr = FontRepository.openFont(fontConfig.timesNewRoman)
        for ((key,value) in replaceValues){
            val textFragmentAbsorber = TextFragmentAbsorber(key)
            val textFragmentCollection = textFragmentAbsorber.textFragments
            document.pages.accept(textFragmentAbsorber)
            for (textFragment in textFragmentCollection) {
                textFragment.textState.font = tnr
                textFragment.text = value
            }
        }
        return document
    }
}