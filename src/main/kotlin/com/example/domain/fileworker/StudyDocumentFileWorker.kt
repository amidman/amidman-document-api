package com.example.domain.fileworker

import com.aspose.pdf.Document
import com.example.domain.fileworker.basic.FileWorker
import com.example.domain.model.config.FileConfig
import com.example.domain.model.config.FontConfig
import com.example.domain.model.fileworker.StudyDocumentData
import com.example.domain.model.group.GroupStatus
import com.example.domain.model.student.StudentModel
import com.example.domain.model.student_request.StudentRequestDTO
import com.example.domain.model.user.fio
import com.example.domain.utils.toHumanFormat
import kotlinx.datetime.LocalDate
import kotlinx.datetime.toKotlinLocalDate



class StudyDocumentFileWorker(
    private val fileConfig:FileConfig,
    override val fontConfig: FontConfig,
):FileWorker() {

    fun replaceValues(data: StudyDocumentData):HashMap<String,String> =
        hashMapOf(
            "{00.00.0000}" to data.currentDate.toHumanFormat(),
            "{000}" to "${data.model.department.departmentShortName.first()}-${data.number}",
            "{00000000000000000000}" to data.model.user.fio(),
            "{00.00.1100}" to data.studyStartDate.toHumanFormat(),
            "{11.11.1111}" to data.studyEndDate.toHumanFormat(),
            "{number}" to data.model.document.orderNumber,
            "{22.22.2222}" to data.model.document.orderDate.toHumanFormat(),
            "{ADDED INFO}" to data.model.addedInfo,
        )


    fun studentModelToStudyDocumentData(request:StudentRequestDTO,model: StudentModel,currentDate: LocalDate):StudyDocumentData{
        val group = model.group
        val document = model.document
        val studyLastYear = document.studyStartDate.year + group.maxYearCount
        val studyEndDate = java.time.LocalDate.of(studyLastYear,6,30)
        return StudyDocumentData(
            currentDate = currentDate,
            number = request.id ?: 0,
            studyStartDate = document.studyStartDate,
            studyEndDate = studyEndDate.toKotlinLocalDate(),
            model = model,
        )
    }
    override suspend fun generateFile(currentDate: LocalDate, request: StudentRequestDTO, studentModel: StudentModel): Document {
        val documentData = studentModelToStudyDocumentData(
            request = request,
            model = studentModel,
            currentDate = currentDate
        )
        val replaceValues = replaceValues(documentData)
        return when(studentModel.group.status){
            GroupStatus.BUDGET -> replaceValuesInFile(fileConfig.budgetStudyDocument,replaceValues)
            GroupStatus.NOBUGET -> replaceValuesInFile(fileConfig.nobudgetStudyDocument,replaceValues)
        }
    }


}