package com.example.domain.dao

import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.dao.basic.IBaseDao
import com.example.domain.model.group.GroupDTO
import kotlinx.coroutines.Deferred

interface GroupDao:IBaseDao<GroupDTO> {

    suspend fun isGroupNameUnique(groupName:String):Boolean

    suspend fun findByGroupNameAsync(groupName: String): Deferred<GroupDTO?>

    suspend fun getAllByDepartmentIdAsync(departmentId:Int): Deferred<List<GroupDTO>>

    suspend fun getAllByDepartmentNameAsync(departmentName:String): Deferred<List<GroupDTO>>

    suspend fun updateGroupNameAsync(groupId:Int,newGroupName: String):Deferred<Int>

    suspend fun updateGroupYearCountAsync(groupId: Int,newYearCount:Int):Deferred<Int>

    suspend fun updateGroupMaxYearCountAsync(groupId: Int,newMaxYearCount:Int):Deferred<Int>
}