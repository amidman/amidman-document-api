package com.example.domain.dao

import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.dao.basic.IBaseDao
import com.example.domain.model.student_request.StudentRequestDTO
import com.example.domain.model.student_request.StudentRequestStatus
import kotlinx.coroutines.Deferred
import kotlinx.datetime.LocalDate

interface StudentRequestDao : IBaseDao<StudentRequestDTO> {

    suspend fun updateRequestStatusAsync(
        requestId: Int,
        newRequestStatus: StudentRequestStatus
    ): Deferred<Int>

    suspend fun getByDepartmentIdAsync(departmentId: Int): Deferred<List<StudentRequestDTO>>

    suspend fun getAllNotAcceptedByDepartmentIdAsync(departmentId: Int): Deferred<List<StudentRequestDTO>>

    suspend fun getByStudentIdAsync(studentId: Int): Deferred<List<StudentRequestDTO>>

    suspend fun getAcceptedListInDateRangeAsync(
        departmentId: Int,
        startDate: LocalDate,
        endDate: LocalDate
    ): Deferred<List<StudentRequestDTO>>

    suspend fun getLastNRequestsAsync(
        departmentId: Int,
        count: Int,
        offset: Long = 0
    ): Deferred<List<StudentRequestDTO>>

}