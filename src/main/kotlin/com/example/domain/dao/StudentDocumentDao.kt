package com.example.domain.dao

import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.dao.basic.IBaseDao
import com.example.domain.model.student_document.StudentDocumentDTO
import kotlinx.coroutines.Deferred

interface StudentDocumentDao:IBaseDao<StudentDocumentDTO> {

    suspend fun changeDocumentNumberAsync(docId:Int,newDocNumber:String): Deferred<Int>

    suspend fun changeOrderNumberAsync(docId:Int,newOrderNumber:String):Deferred<Int>

    suspend fun findByDocNumberAsync(docNumber:String):Deferred<StudentDocumentDTO?>

    suspend fun findByOrderNumberAsync(orderNumber:String):Deferred<List<StudentDocumentDTO>>

    suspend fun isDocNumberUnique(docNumber: String):Boolean
}