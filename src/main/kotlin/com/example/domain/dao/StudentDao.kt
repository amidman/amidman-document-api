package com.example.domain.dao

import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.dao.basic.IBaseDao
import com.example.domain.model.student.StudentDTO
import com.example.domain.model.student.StudentModel
import kotlinx.coroutines.Deferred

interface StudentDao: IBaseDao<StudentDTO> {
    suspend fun findByUserIdAsync(userId:Int): Deferred<StudentDTO?>

    suspend fun findStudentModelAsync(studentId:Int):Deferred<StudentModel?>

    suspend fun getAllStudentModelAsync(): Deferred<List<StudentModel>>

    suspend fun getAllStudentModelByGroupAsync(groupId: Int):Deferred<List<StudentModel>>

    suspend fun getAllStudentModelByGroupNameAsync(groupName:String):Deferred<List<StudentModel>>

    suspend fun getAllStudentModelByDepartmentAsync(departmentId:Int):Deferred<List<StudentModel>>

    suspend fun updateGroupAsync(studentId: Int,groupId:Int):Deferred<Int>

    suspend fun updateAddedInfoAsync(studentId: Int,addedInfo:String):Deferred<Int>


}