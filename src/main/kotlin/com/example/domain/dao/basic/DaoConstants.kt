package com.example.domain.dao.basic

object DaoConstants {
    const val userTableName = "Users"
    const val studentTableName = "Students"
    const val groupTableName = "Groups"
    const val studentDocumentTableName = "StudentDocuments"
    const val departmentTableName = "Departments"
    const val studentRequestTableName = "StudentRequests"
}