package com.example.domain.dao.basic

import com.example.errors.EntityError
import com.example.errors.entity_errors.BasicErrors
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.statements.UpdateStatement
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

abstract class BaseDao<T>(name:String): Table(name),IBaseDao<T> {

    protected abstract val wrapper: BaseDaoWrapper<T>

    val id = integer("id",).autoIncrement()

    override val primaryKey: PrimaryKey
        get() = PrimaryKey(id)

    protected abstract fun insertStatement(dto:T):InsertStatement<Number>

    override suspend fun createAsync(dto:T):Deferred<Int> = suspendedTransactionAsync{
        insertStatement(dto)[this@BaseDao.id]
    }

    override suspend fun deleteAsync(entityId:Int) = suspendedTransactionAsync {
        deleteWhere { this@BaseDao.id eq entityId }
    }

    override suspend fun findByIdAsync(entityId: Int): Deferred<T?> = suspendedTransactionAsync {
        select {
            this@BaseDao.id eq entityId
        }.mapNotNull {
            wrapper.fromResultRow(it)
        }.singleOrNull()
    }


    override suspend fun getAllAsync():Deferred<List<T>> = suspendedTransactionAsync{
        selectAll().mapNotNull {
            wrapper.fromResultRow(it)
        }
    }

    override suspend fun count():Int = newSuspendedTransaction {
        selectAll().count().toInt()
    }

    protected suspend fun updateWhereAsync(
        where:SqlExpressionBuilder.()->Op<Boolean>,
        body: BaseDao<T>.(UpdateStatement) -> Unit
    ) = suspendedTransactionAsync{
        update(
            where = where,
            body = body
        )
    }
    protected suspend fun updateByIdAsync(
        id:Int,
        body: BaseDao<T>.(UpdateStatement) -> Unit
    ) = updateWhereAsync(
        where = {
            this@BaseDao.id eq id
        },
        body = body
    )


    protected suspend fun findSingleAsync(where: SqlExpressionBuilder.() -> Op<Boolean>):Deferred<T?> = suspendedTransactionAsync{
        select(
            where = where
        ).mapNotNull {
            wrapper.fromResultRow(it)
        }.singleOrNull()
    }

    protected suspend fun findSingleAsync(body:Op<Boolean>):Deferred<T?> = suspendedTransactionAsync{
        select(body).mapNotNull {
            wrapper.fromResultRow(it)
        }.singleOrNull()
    }

    protected suspend fun findManyAsync(
        body:Op<Boolean>
    ):Deferred<List<T>> = suspendedTransactionAsync {
        select(
            where = body
        ).mapNotNull {
            wrapper.fromResultRow(it)
        }
    }

    protected suspend fun findManyAsync(
        where: SqlExpressionBuilder.() -> Op<Boolean>
    ):Deferred<List<T>> = suspendedTransactionAsync {
        select(
            where = where
        ).mapNotNull {
            wrapper.fromResultRow(it)
        }
    }

    protected suspend fun findManyAsync(
        where: SqlExpressionBuilder.() -> Op<Boolean>,
        n:Int,
        offset:Long = 0,
    ):Deferred<List<T>> = suspendedTransactionAsync{
        select(
            where = where
        ).limit(
            n = n,
            offset = offset
        ).mapNotNull {
            wrapper.fromResultRow(it)
        }
    }

    protected suspend fun findManyAsync(
        where:Op<Boolean>,
        n:Int,
        offset:Long = 0,
    ):Deferred<List<T>> = suspendedTransactionAsync {
        select(
            where = where
        ).limit(
            n = n,
            offset = offset
        ).mapNotNull {
            wrapper.fromResultRow(it)
        }
    }






}