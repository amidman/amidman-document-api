package com.example.domain.dao.basic

import org.jetbrains.exposed.sql.ResultRow

interface BaseDaoWrapper<T> {
    fun fromResultRow(row:ResultRow):T
}