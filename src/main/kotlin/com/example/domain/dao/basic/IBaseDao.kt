package com.example.domain.dao.basic

import kotlinx.coroutines.Deferred

interface IBaseDao<T> {
    suspend fun createAsync(dto: T): Deferred<Int>

    suspend fun deleteAsync(entityId: Int): Deferred<Int>

    suspend fun findByIdAsync(entityId: Int): Deferred<T?>

    suspend fun getAllAsync(): Deferred<List<T>>

    suspend fun count(): Int
}