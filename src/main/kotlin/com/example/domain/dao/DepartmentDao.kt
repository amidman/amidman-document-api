package com.example.domain.dao

import com.example.domain.dao.basic.IBaseDao
import com.example.domain.model.department.DepartmentDTO
import kotlinx.coroutines.Deferred

interface DepartmentDao:IBaseDao<DepartmentDTO> {

    suspend fun findByDepartmentNameAsync(departmentName:String): Deferred<DepartmentDTO?>

    suspend fun isDepartmentNameUnique(departmentName: String):Boolean

    suspend fun updateDepartmentNameAsync(departmentId:Int,departmentName: String):Deferred<Int>

    suspend fun updateShortNameAsync(departmentId:Int,newShortName:String):Deferred<Int>

}