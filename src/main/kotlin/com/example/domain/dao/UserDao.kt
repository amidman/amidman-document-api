package com.example.domain.dao

import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.dao.basic.IBaseDao
import com.example.domain.model.user.UserDTO
import com.example.domain.model.user.UserFIO
import kotlinx.coroutines.Deferred

interface UserDao: IBaseDao<UserDTO>{
    suspend fun updateNameAsync(id:Int,name:String): Deferred<Int>

    suspend fun updateSurnameAsync(id:Int,surname:String): Deferred<Int>

    suspend fun updateFatherNameAsync(id:Int,fatherName:String): Deferred<Int>

    suspend fun updateLoginAsync(id:Int,login:String): Deferred<Int>

    suspend fun updatePasswordAsync(id:Int,password:String): Deferred<Int>

    suspend fun updateRoleAsync(id:Int,role:String):Deferred<Int>

    suspend fun updateEmailAsync(id:Int,email:String):Deferred<Int>

    suspend fun findByLoginAsync(login: String):Deferred<UserDTO?>

    suspend fun updateFIOAsync(userId:Int,userFio:UserFIO):Deferred<Int>

    suspend fun isLoginUnique(login: String):Boolean

    suspend fun isEmailUnique(email:String):Boolean
}