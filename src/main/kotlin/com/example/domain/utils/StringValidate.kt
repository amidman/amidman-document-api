package com.example.domain.utils

import com.example.errors.entity_errors.BasicErrors

fun String.validate(maxLength:Int,fieldName:String = "",){
    if (isEmpty() || isBlank())
        throw BasicErrors.EMPTY_FIELDS
    if (length > maxLength)
        throw BasicErrors.MANY_SYMBOLS(fieldName,maxLength)
}