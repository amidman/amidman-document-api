package com.example.domain.utils

import at.favre.lib.crypto.bcrypt.BCrypt
import com.example.errors.entity_errors.UserErrors

object PasswordHashing {
    fun hashPassword(password: String): String = BCrypt.withDefaults().hashToString(4, password.toCharArray())

    fun checkPassword(hashPassword: String, userPassword: String) {
        val verified = BCrypt.verifyer().verify(userPassword.toCharArray(), hashPassword.toCharArray()).verified
        if (!verified)
            throw UserErrors.WRONG_PASSWORD
    }
}