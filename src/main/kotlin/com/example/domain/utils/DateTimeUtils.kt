package com.example.domain.utils

import kotlinx.datetime.LocalDate


private fun Int.toDoubleDigitFormat():String = String.format("%02d",this)

fun LocalDate.toHumanFormat():String {
    return "${dayOfMonth.toDoubleDigitFormat()}.${monthNumber.toDoubleDigitFormat()}.$year"
}
