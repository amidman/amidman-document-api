package com.example.domain.utils

import com.example.errors.entity_errors.UserErrors
import org.apache.commons.validator.routines.EmailValidator
import java.util.regex.Pattern

fun String.checkEmail(){
    val result = EmailValidator.getInstance().isValid(this)
    if (!result)
        throw UserErrors.WRONG_EMAIL
}