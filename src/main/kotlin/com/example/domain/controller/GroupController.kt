package com.example.domain.controller

import com.example.domain.controller.basic.BaseController
import com.example.domain.dao.DepartmentDao
import com.example.domain.dao.GroupDao
import com.example.domain.dao.StudentDao
import com.example.domain.model.group.GroupConstants
import com.example.domain.model.group.GroupDTO
import com.example.domain.model.group.GroupModel
import com.example.errors.entity_errors.DepartmentErrors
import com.example.errors.entity_errors.GroupErrors

class GroupController(
    private val groupDao: GroupDao,
    private val departmentDao: DepartmentDao,
    private val studentDao: StudentDao,
) : BaseController<GroupDTO>(
    dao = groupDao, entityErrors = GroupErrors
) {

    private suspend fun checkGroupNameUnique(groupName: String) {
        if (!groupDao.isGroupNameUnique(groupName)) throw GroupErrors.WRONG_NAME
    }

    private fun checkGroupYearCount(yearCount: Int) {
        if (yearCount !in GroupConstants.yearCountRange) throw GroupErrors.WRONG_GROUP_YEAR_COUNT
    }

    private fun checkGroupMaxYearCount(maxYearCount: Int) {
        if (maxYearCount !in GroupConstants.maxYearCountRange) throw GroupErrors.WRONG_GROUP_MAX_YEAR_COUNT
    }

    private suspend fun checkDepartmentExist(departmentId: Int) {
        departmentDao.findByIdAsync(departmentId).await() ?: throw DepartmentErrors.NOT_FOUND
    }

    suspend fun createGroup(group: GroupDTO): GroupDTO {
        checkGroupNameUnique(group.groupName)
        checkDepartmentExist(group.departmentId)
        val groupId = groupDao.createAsync(group).await()
        return group.copy(id = groupId)
    }

    suspend fun changeName(groupId: Int, groupName: String) {
        findById(groupId)
        checkGroupNameUnique(groupName)
        groupDao.updateGroupNameAsync(groupId, groupName).await()
    }

    suspend fun changeYearCount(groupId: Int, newYearCount: Int) {
        findById(groupId)
        checkGroupYearCount(newYearCount)
        groupDao.updateGroupYearCountAsync(groupId = groupId, newYearCount = newYearCount).await()
    }

    suspend fun changeMaxYearCount(groupId: Int, maxYearCount: Int) {
        findById(groupId)
        checkGroupMaxYearCount(maxYearCount)
        groupDao.updateGroupMaxYearCountAsync(groupId, maxYearCount).await()
    }

    suspend fun findGroupModelById(groupId: Int): GroupModel {
        val group = findById(groupId)
        val studentList = studentDao.getAllStudentModelByGroupAsync(groupId)
        val department = departmentDao.findByIdAsync(group.departmentId).await() ?: throw DepartmentErrors.NOT_FOUND
        return GroupModel(
            group = group, studentList = studentList.await(), department = department
        )
    }

    suspend fun findByGroupModelByName(groupName: String): GroupModel {
        val group = groupDao.findByGroupNameAsync(groupName).await()
            ?: throw GroupErrors.NOT_FOUND
        val studentList = studentDao
            .getAllStudentModelByGroupNameAsync(groupName)
        val department = departmentDao.findByIdAsync(group.departmentId).await()
            ?: throw DepartmentErrors.NOT_FOUND
        return GroupModel(
            group = group, studentList = studentList.await(), department = department
        )
    }


}