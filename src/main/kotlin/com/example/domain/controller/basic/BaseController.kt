package com.example.domain.controller.basic

import com.example.domain.dao.basic.IBaseDao
import com.example.errors.EntityError

abstract class BaseController<T>(
    private val dao: IBaseDao<T>,
    private val entityErrors:EntityError
) {

    suspend fun getAll():List<T> = dao.getAllAsync().await()

    suspend fun findById(entityId:Int):T =
        dao.findByIdAsync(entityId).await() ?: throw entityErrors.NOT_FOUND

    open suspend fun delete(entityId: Int){
        findById(entityId)
        dao.deleteAsync(entityId).await()
    }


}