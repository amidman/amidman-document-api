package com.example.domain.controller

import com.aspose.pdf.Document
import com.example.domain.controller.basic.BaseController
import com.example.domain.dao.DepartmentDao
import com.example.domain.dao.GroupDao
import com.example.domain.dao.StudentDao
import com.example.domain.dao.StudentRequestDao
import com.example.domain.fileworker.basic.MainFileWorker
import com.example.domain.model.student_request.*
import com.example.errors.entity_errors.DepartmentErrors
import com.example.errors.entity_errors.GroupErrors
import com.example.errors.entity_errors.StudentErrors
import com.example.errors.entity_errors.StudentRequestErrors
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import kotlinx.datetime.LocalDate

class StudentRequestController(
    private val studentRequestDao: StudentRequestDao,
    private val studentDao: StudentDao,
    private val mainFileWorker: MainFileWorker,
    private val departmentDao: DepartmentDao,
    private val groupDao: GroupDao,
) : BaseController<StudentRequestDTO>(
    dao = studentRequestDao,
    entityErrors = StudentRequestErrors
) {

    suspend fun checkStudentExist(studentId: Int) {
        studentDao.findByIdAsync(studentId).await() ?: throw StudentErrors.NOT_FOUND
    }

    suspend fun checkDepartmentExist(departmentId: Int) {
        departmentDao.findByIdAsync(departmentId).await() ?: throw DepartmentErrors.NOT_FOUND
    }

    suspend fun checkRequestDate(studentId: Int, requestDate: LocalDate, requestType: StudentRequestType) {
        val requestList = getRequestListByStudent(studentId)
        if (requestList.isEmpty())
            return
        val filteredRequestList = requestList.filter { it.type == requestType }
        val prevDate = filteredRequestList.maxOf { rqst -> rqst.date.toEpochDays() }
        if ((requestDate.toEpochDays() - prevDate) < 7)
            throw StudentRequestErrors.SEND_LATER
    }

    suspend fun createStudentRequest(studentId: Int, createRequestModel: RequestCreateModel): StudentRequestDTO {
        val student = studentDao.findByIdAsync(studentId).await() ?: throw StudentErrors.NOT_FOUND
        checkRequestDate(studentId, createRequestModel.date, createRequestModel.type)
        val group = groupDao.findByIdAsync(student.groupId).await() ?: throw GroupErrors.NOT_FOUND
        val departmentId = group.departmentId
        val status = StudentRequestStatus.SEND
        val requestDTO = StudentRequestDTO(
            id = null,
            studentId = studentId,
            status = status,
            type = createRequestModel.type,
            count = createRequestModel.count,
            date = createRequestModel.date,
            departmentId = departmentId
        )
        val requestId = studentRequestDao.createAsync(requestDTO).await()
        return findById(requestId)
    }

    suspend fun getDocumentFromRequest(requestId: Int, currentDate: LocalDate): Document {
        val request = findById(requestId)
        val studentModel = studentDao.findStudentModelAsync(request.studentId).await() ?: throw StudentErrors.NOT_FOUND
        return mainFileWorker.handleRequestType(
            request = request,
            studentModel = studentModel,
            currentDate = currentDate
        )
    }

    suspend fun acceptRequest(requestId: Int) {
        val request = findById(requestId)
        if (request.status == StudentRequestStatus.ACCEPTED)
            throw StudentRequestErrors.ALREADY_ACCEPTED
        val newRequestStatus = StudentRequestStatus.ACCEPTED
        studentRequestDao.updateRequestStatusAsync(requestId, newRequestStatus).await()
    }

    suspend fun mapRequestDTOListToModelList(requestDTOList: List<StudentRequestDTO>): List<RequestModel> =
        withContext(Dispatchers.IO) {
            requestDTOList.map { requestDTO ->
                async {
                    val studentModel =
                        studentDao.findStudentModelAsync(requestDTO.studentId)
                            .await() ?: throw StudentErrors.NOT_FOUND
                    RequestModel(
                        request = requestDTO,
                        student = studentModel,
                    )
                }
            }.map { it.await() }
        }

    suspend fun getRequestListByDepartment(departmentId: Int): List<RequestModel> {
        checkDepartmentExist(departmentId)
        val requestList = studentRequestDao.getByDepartmentIdAsync(departmentId).await()
        return mapRequestDTOListToModelList(requestList)
    }

    suspend fun getAllNotAcceptedByDepartment(departmentId: Int): List<RequestModel> {
        checkDepartmentExist(departmentId)
        val requestList = studentRequestDao.getAllNotAcceptedByDepartmentIdAsync(departmentId).await()
        return mapRequestDTOListToModelList(requestList)
    }

    suspend fun getAllAcceptedByDate(
        departmentId: Int,
        startDate: LocalDate,
        endDate: LocalDate,
    ): List<RequestModel> {
        checkDepartmentExist(departmentId)
        val requestList = studentRequestDao.getAcceptedListInDateRangeAsync(
            departmentId = departmentId,
            startDate = startDate,
            endDate = endDate
        ).await()

        return mapRequestDTOListToModelList(requestList)
    }

    suspend fun getRequestListByStudent(studentId: Int): List<StudentRequestDTO> {
        checkStudentExist(studentId)
        return studentRequestDao.getByStudentIdAsync(studentId).await()
    }

    override suspend fun delete(entityId: Int) {
        val request = findById(entityId)
        if (request.status == StudentRequestStatus.SEND)
            studentRequestDao.deleteAsync(entityId).await()
        else
            throw StudentRequestErrors.DELETE_ACCEPTED
    }

}

