package com.example.domain.controller

import com.example.domain.controller.basic.BaseController
import com.example.domain.dao.DepartmentDao
import com.example.domain.dao.GroupDao
import com.example.domain.model.department.DepartmentDTO
import com.example.domain.model.department.DepartmentModel
import com.example.errors.entity_errors.BasicErrors
import com.example.errors.entity_errors.DepartmentErrors
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class DepartmentController(
    private val departmentDao: DepartmentDao,
    private val groupDao: GroupDao,
) : BaseController<DepartmentDTO>(
    dao = departmentDao,
    entityErrors = DepartmentErrors
) {


    suspend fun mapDepartmentDTOListToModel(departmentDTOList: List<DepartmentDTO>): List<DepartmentModel> =
        withContext(Dispatchers.IO) {
            departmentDTOList.map { departmentDTO ->
                async {
                    val departmentId = departmentDTO.id ?: throw BasicErrors.DATABASE_ERROR
                    val groupList = groupDao.getAllByDepartmentIdAsync(departmentId).await()
                    DepartmentModel(
                        department = departmentDTO,
                        groupList = groupList
                    )
                }
            }.map { it.await() }
        }


    suspend fun checkDepartmentNameUnique(departmentName: String) {
        if (!departmentDao.isDepartmentNameUnique(departmentName))
            throw DepartmentErrors.WRONG_NAME
    }

    suspend fun createDepartment(departmentDTO: DepartmentDTO): DepartmentDTO {
        checkDepartmentNameUnique(departmentDTO.departmentName)
        val departmentId = departmentDao.createAsync(departmentDTO).await()
        return departmentDTO.copy(id = departmentId)
    }

    suspend fun changeDepartmentName(departmentId: Int, departmentName: String) {
        findById(departmentId)
        checkDepartmentNameUnique(departmentName)
        departmentDao.updateDepartmentNameAsync(departmentId, departmentName).await()
    }

    suspend fun changeDepartmentShortName(departmentId: Int, departmentShortName: String) {
        findById(departmentId)
        departmentDao.updateShortNameAsync(departmentId, departmentShortName).await()
    }

    suspend fun getAllDepartmentModel(): List<DepartmentModel> {
        return mapDepartmentDTOListToModel(getAll())
    }

    suspend fun findDepartmentModelById(departmentId: Int): DepartmentModel {
        val department = findById(departmentId)
        val groupList = groupDao.getAllByDepartmentIdAsync(departmentId)
        return DepartmentModel(
            department = department,
            groupList = groupList.await()
        )
    }

    suspend fun findDepartmentModelByName(departmentName: String): DepartmentModel {
        val department =
            departmentDao.findByDepartmentNameAsync(departmentName)
                .await() ?: throw DepartmentErrors.NOT_FOUND
        val groupList = groupDao.getAllByDepartmentNameAsync(departmentName)
        return DepartmentModel(
            department = department,
            groupList = groupList.await()
        )
    }

}