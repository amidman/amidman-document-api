package com.example.domain.controller

import com.example.domain.controller.basic.BaseController
import com.example.domain.dao.*
import com.example.domain.model.student.StudentCreateModel
import com.example.domain.model.student.StudentDTO
import com.example.domain.model.student.StudentModel
import com.example.domain.model.user.UserDTO
import com.example.domain.utils.PasswordHashing
import com.example.errors.entity_errors.*
import com.example.security.middleware.ROLES
import kotlinx.coroutines.awaitAll

class StudentController(
    private val studentDao: StudentDao,
    private val groupDao: GroupDao,
    private val studentDocumentDao: StudentDocumentDao,
    private val userDao: UserDao,
    private val departmentDao: DepartmentDao,
) : BaseController<StudentDTO>(
    dao = studentDao,
    entityErrors = StudentErrors
) {

    private suspend fun checkDocNumberUnique(docNumber: String) {
        if (!studentDocumentDao.isDocNumberUnique(docNumber))
            throw StudentDocumentErrors.WRONG_DOC_NUMBER
    }

    private suspend fun checkGroupExist(groupId: Int) {
        groupDao.findByIdAsync(groupId).await() ?: throw GroupErrors.NOT_FOUND
    }

    private suspend fun checkDepartmentExist(departmentId: Int) {
        departmentDao.findByIdAsync(departmentId).await() ?: throw DepartmentErrors.NOT_FOUND
    }


    companion object {

        fun generateUserFromStudentCreateModel(model: StudentCreateModel): UserDTO {
            val name = model.fio.name
            val surname = model.fio.surname
            val fatherName = model.fio.fatherName
            val login = surname + name.first().uppercaseChar() + (fatherName.takeIf { it.isNotEmpty() }?.first()?.uppercaseChar() ?: "")
            val password = PasswordHashing.hashPassword(login)
            val role = ROLES.STUDENT
            return UserDTO(
                name = name,
                surname = surname,
                fatherName = fatherName,
                login = login,
                password = password,
                role = role,
            )
        }

    }

    suspend fun createStudent(model: StudentCreateModel): StudentModel {
        checkDocNumberUnique(model.document.docNumber)
        val group = groupDao.findByGroupNameAsync(model.groupName).await() ?: throw GroupErrors.NOT_FOUND
        val groupId = group.id ?: throw BasicErrors.DATABASE_ERROR
        val documentId = studentDocumentDao.createAsync(model.document)
        val user = generateUserFromStudentCreateModel(model)
        val userId = userDao.createAsync(user)
        val studentId = studentDao.createAsync(
            StudentDTO(
                userId = userId.await(),
                groupId = groupId,
                studentDocumentId = documentId.await(),
                addedInfo = model.addedInfo
            )
        ).await()
        val studentModel = findStudentModelById(studentId)
        if (
            studentModel.group.id == groupId &&
            studentModel.document.id == documentId.await() &&
            studentModel.user.id == userId.await()
        ) {
            return studentModel
        } else {
            awaitAll(
                studentDocumentDao.deleteAsync(documentId.await()),
                userDao.deleteAsync(userId.await()),
                studentDao.deleteAsync(studentId)
            )
            throw BasicErrors.DATABASE_ERROR
        }
    }

    suspend fun getAllStudentModel(): List<StudentModel> = studentDao.getAllStudentModelAsync().await()

    suspend fun getAllStudentModelByGroup(groupId: Int): List<StudentModel> {
        checkGroupExist(groupId)
        return studentDao.getAllStudentModelByGroupAsync(groupId).await()
    }

    suspend fun getAllStudentModelByDepartment(departmentId: Int): List<StudentModel> {
        checkDepartmentExist(departmentId)
        return studentDao.getAllStudentModelByDepartmentAsync(departmentId).await()
    }

    override suspend fun delete(entityId: Int) {
        val student = findById(entityId)
        awaitAll(
            studentDocumentDao.deleteAsync(student.studentDocumentId),
            userDao.deleteAsync(student.userId),
        )
    }

    suspend fun changeGroup(studentId: Int, groupId: Int) {
        findById(studentId)
        checkGroupExist(groupId)
        studentDao.updateGroupAsync(studentId = studentId, groupId = groupId).await()
    }

    suspend fun changeAddedInfo(studentId: Int, newAddedInfo: String) {
        findById(studentId)
        studentDao.updateAddedInfoAsync(studentId, newAddedInfo).await()
    }

    suspend fun findStudentModelById(studentId: Int): StudentModel =
        studentDao.findStudentModelAsync(studentId).await() ?: throw StudentErrors.NOT_FOUND


}