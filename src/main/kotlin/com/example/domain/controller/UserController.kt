package com.example.domain.controller

import com.example.domain.controller.basic.BaseController
import com.example.domain.dao.StudentDao
import com.example.domain.dao.UserDao
import com.example.domain.model.auth.LoginData
import com.example.domain.model.auth.TokenBody
import com.example.domain.model.user.UserDTO
import com.example.domain.model.user.UserFIO
import com.example.domain.utils.PasswordHashing
import com.example.domain.utils.checkEmail
import com.example.errors.entity_errors.BasicErrors
import com.example.errors.entity_errors.StudentErrors
import com.example.errors.entity_errors.UserErrors
import com.example.security.middleware.ROLES
import com.example.security.middleware.TokenProvider

class UserController(
    private val userDao: UserDao,
    private val studentDao: StudentDao,
    private val tokenProvider: TokenProvider
) : BaseController<UserDTO>(
    dao = userDao,
    entityErrors = UserErrors
) {

    private suspend fun checkLoginUnique(login: String) {
        if (!userDao.isLoginUnique(login))
            throw UserErrors.WRONG_LOGIN
    }

    private suspend fun checkEmailUnique(email:String){
        if (!userDao.isEmailUnique(email))
            throw UserErrors.EMAIL_UNIQUE
    }

    private fun checkIsRoleExist(role: String) {
        try {
            ROLES.valueOf(role)
        } catch (_: Exception) {
            throw UserErrors.WRONG_ROLE
        }
    }

    suspend fun registerUser(user: UserDTO): UserDTO {
        checkLoginUnique(user.login)
        user.email?.let { checkEmailUnique(it) }
        val hashPassword = PasswordHashing.hashPassword(password = user.password)
        val userDTO = user.copy(password = hashPassword)
        val userId = userDao.createAsync(userDTO).await()
        return findById(userId)
    }

    suspend fun auth(loginData: LoginData): TokenBody {
        val user = userDao.findByLoginAsync(login = loginData.login)
            .await() ?: throw UserErrors.NOT_FOUND
        PasswordHashing.checkPassword(user.password, loginData.password)
        val userId = user.id ?: throw BasicErrors.DATABASE_ERROR
        val token = when (user.role) {
            ROLES.STUDENT -> {
                val student = studentDao.findByUserIdAsync(userId).await() ?: throw StudentErrors.NOT_FOUND
                val studentId = student.id
                    ?: throw BasicErrors.DATABASE_ERROR
                tokenProvider.createStudentToken(userId = userId, studentId = studentId)
            }

            ROLES.ADMIN -> tokenProvider.createAdminToken(userId)
            ROLES.SECRETARY -> tokenProvider.createSecretaryToken(userId)
        }
        return TokenBody(
            token = token,
            role = user.role
        )
    }

    suspend fun changeLogin(userId: Int, newLogin: String) {
        checkLoginUnique(newLogin)
        findById(userId)
        userDao.updateLoginAsync(userId, newLogin).await()
    }

    suspend fun changePassword(userId: Int, newPassword: String) {
        findById(userId)
        val password = PasswordHashing.hashPassword(newPassword)
        userDao.updatePasswordAsync(userId, password).await()
    }

    suspend fun changeUserRole(userId: Int, newRole: String) {
        checkIsRoleExist(newRole)
        findById(userId)
        userDao.updateRoleAsync(userId, newRole).await()
    }

    suspend fun changeName(userId: Int, newName: String) {
        findById(userId)
        userDao.updateNameAsync(userId, newName).await()
    }

    suspend fun changeSurname(userId: Int, newSurname: String) {
        findById(userId)
        userDao.updateSurnameAsync(userId, newSurname).await()
    }

    suspend fun changeFatherName(userId: Int, newFatherName: String) {
        findById(userId)
        userDao.updateFatherNameAsync(userId, newFatherName).await()
    }

    suspend fun changeEmail(userId: Int,newEmail:String){
        findById(userId)
        newEmail.checkEmail()
        userDao.updateEmailAsync(userId,newEmail).await()
    }

    suspend fun changeFio(userId: Int, userFio: UserFIO) {
        findById(userId)
        userDao.updateFIOAsync(userId, userFio).await()
    }


}