package com.example.domain.model.auth

import com.example.security.middleware.ROLES
import kotlinx.serialization.Serializable

@Serializable
data class TokenBody(
    val token:String,
    val role:ROLES,
)
