package com.example.domain.model.auth

import com.example.domain.utils.validate
import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class LoginData(
    @Serializable(with = TrimStringSerializer::class)
    val login:String,
    @Serializable(with = TrimStringSerializer::class)
    val password:String,
)
