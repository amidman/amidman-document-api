package com.example.domain.model.fileworker

import com.example.domain.model.student.StudentModel
import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable


@Serializable
data class StudyDocumentData(
    val currentDate:LocalDate,
    val number:Int,
    val studyStartDate: LocalDate,
    val studyEndDate:LocalDate,
    val model:StudentModel
)
