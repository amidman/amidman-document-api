package com.example.domain.model.user

import com.example.domain.utils.checkEmail
import com.example.domain.utils.validate
import com.example.errors.entity_errors.BasicErrors
import com.example.security.middleware.ROLES
import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class UserDTO(
    val id: Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val login: String,
    @Serializable(with = TrimStringSerializer::class)
    val password: String,
    @Serializable(with = TrimStringSerializer::class)
    val name: String,
    @Serializable(with = TrimStringSerializer::class)
    val surname: String,
    @Serializable(with = TrimStringSerializer::class)
    val fatherName: String,
    @Serializable(with = TrimStringSerializer::class)
    val email: String? = null,
    val role: ROLES
)

fun UserDTO.fio() = "$surname $name $fatherName"

fun UserDTO.validate() {
    login.validate(maxLength = UserConstants.LOGIN_FIELD_SIZE, fieldName = UserConstants.LOGIN_FIELD)
    password.validate(maxLength = UserConstants.PASSWORD_FIELD_SIZE, fieldName = UserConstants.PASSWORD_FIELD)
    name.validate(maxLength = UserConstants.NAME_FIELD_SIZE, fieldName = UserConstants.NAME_FIELD)
    surname.validate(maxLength = UserConstants.SURNAME_FIELD_SIZE, fieldName = UserConstants.SURNAME_FIELD)
    email?.validate(maxLength = UserConstants.EMAIL_FIELD_SIZE, fieldName = UserConstants.EMAIL_NAME_FIELD)
    email?.checkEmail()
    if (fatherName.length > UserConstants.FATHER_NAME_FIELD_SIZE)
        throw BasicErrors.MANY_SYMBOLS(
            fieldName = UserConstants.FATHER_NAME_FIELD,
            size = UserConstants.FATHER_NAME_FIELD_SIZE
        )
}



