package com.example.domain.model.user

import com.example.domain.utils.validate
import com.example.errors.entity_errors.BasicErrors
import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class UserFIO(
    @Serializable(with = TrimStringSerializer::class)
    val name:String,
    @Serializable(with = TrimStringSerializer::class)
    val surname:String,
    @Serializable(with = TrimStringSerializer::class)
    val fatherName:String
)

fun UserFIO.validate(){
    name.validate(maxLength = UserConstants.NAME_FIELD_SIZE, fieldName = UserConstants.NAME_FIELD)
    surname.validate(maxLength = UserConstants.SURNAME_FIELD_SIZE, fieldName = UserConstants.SURNAME_FIELD)
    if (fatherName.length >= 32)
        throw BasicErrors.MANY_SYMBOLS(
            fieldName = UserConstants.FATHER_NAME_FIELD,
            size = UserConstants.FATHER_NAME_FIELD_SIZE
        )
}
