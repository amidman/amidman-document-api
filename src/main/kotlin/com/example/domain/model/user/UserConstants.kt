package com.example.domain.model.user

object UserConstants {

    const val LOGIN_FIELD = "Логин"
    const val LOGIN_FIELD_SIZE = 32

    const val PASSWORD_FIELD = "Пароль"
    const val PASSWORD_FIELD_SIZE = 128

    const val SURNAME_FIELD = "Фамилия"
    const val SURNAME_FIELD_SIZE = 32

    const val NAME_FIELD = "Имя"
    const val NAME_FIELD_SIZE = 32

    const val FATHER_NAME_FIELD = "Отчество"
    const val FATHER_NAME_FIELD_SIZE = 32

    const val EMAIL_NAME_FIELD = "Почта"
    const val EMAIL_FIELD_SIZE = 60
}