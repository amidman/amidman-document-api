package com.example.domain.model.config

data class JWTConfig(
    val secret:String,
    val audience:String,
    val issuer:String,
    val realm:String
)
