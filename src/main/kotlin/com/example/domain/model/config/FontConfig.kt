package com.example.domain.model.config

data class FontConfig(
    val root:String,
    val timesNewRoman:String
)
