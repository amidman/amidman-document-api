package com.example.domain.model.config

data class DatabaseConfig(
    val user:String,
    val password:String,
    val dbUrl:String,
    val driverName:String
)
