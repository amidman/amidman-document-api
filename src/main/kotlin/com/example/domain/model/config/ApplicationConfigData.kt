package com.example.domain.model.config

data class ApplicationConfigData(
    val host:String,
    val port:Int,
    val swagger_host:String,
)