package com.example.domain.model.config

data class FileConfig(
    val root:String,
    val budgetStudyDocument:String,
    val nobudgetStudyDocument:String,
)
