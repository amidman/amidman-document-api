package com.example.domain.model.department

object DepartmentConstants {
    const val NAME_FIELD = "Название отеделения"
    const val NAME_FIELD_SIZE = 128
    const val SHORT_NAME_FIELD = "Короткое название"
    const val SHORT_NAME_FIELD_SIZE = 8
}