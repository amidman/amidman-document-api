package com.example.domain.model.department

import com.example.domain.model.group.GroupDTO
import com.example.domain.model.group.validate
import kotlinx.serialization.Serializable


@Serializable
data class DepartmentModel(
    val department:DepartmentDTO,
    val groupList:List<GroupDTO>
)

fun DepartmentModel.validate(){
    department.validate()
    groupList.forEach { groupDTO -> groupDTO.validate() }
}