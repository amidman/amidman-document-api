package com.example.domain.model.department

import com.example.domain.utils.validate
import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class DepartmentDTO(
    val id:Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val departmentName:String,
    @Serializable(with = TrimStringSerializer::class)
    val departmentShortName:String,
)

fun DepartmentDTO.validate(){
    departmentName.validate(DepartmentConstants.NAME_FIELD_SIZE,DepartmentConstants.NAME_FIELD)
    departmentShortName.validate(DepartmentConstants.SHORT_NAME_FIELD_SIZE,DepartmentConstants.SHORT_NAME_FIELD)
}