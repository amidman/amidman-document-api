package com.example.domain.model.student_document

import com.example.domain.utils.validate
import com.example.serialization.TrimStringSerializer
import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable


@Serializable
data class StudentDocumentDTO(
    val id: Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val docNumber: String,
    @Serializable(with = TrimStringSerializer::class)
    val orderNumber: String,
    val orderDate: LocalDate,
    val studyStartDate: LocalDate,
)

fun StudentDocumentDTO.validate() {
    docNumber.validate(
        maxLength = StudentDocumentConstants.DOC_NUMBER_FIELD_SIZE,
        fieldName = StudentDocumentConstants.DOC_NUMBER_FIELD
    )
    orderNumber.validate(
        maxLength = StudentDocumentConstants.ORDER_NUMBER_FIELD_SIZE,
        fieldName = StudentDocumentConstants.ORDER_NUMBER_FIELD
    )
}
