package com.example.domain.model.student_document

object StudentDocumentConstants {

    const val DOC_NUMBER_FIELD = "Номер студенческого билета"
    const val DOC_NUMBER_FIELD_SIZE = 64

    const val ORDER_NUMBER_FIELD = "Номер приказа"
    const val ORDER_NUMBER_FIELD_SIZE = 64
}