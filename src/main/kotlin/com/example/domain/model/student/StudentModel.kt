package com.example.domain.model.student

import com.example.domain.model.department.DepartmentDTO
import com.example.domain.model.department.validate
import com.example.domain.model.group.GroupDTO
import com.example.domain.model.group.validate
import com.example.domain.model.student_document.StudentDocumentDTO
import com.example.domain.model.student_document.validate
import com.example.domain.model.user.UserDTO
import com.example.domain.model.user.validate
import com.example.domain.utils.validate
import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class StudentModel(
    val id:Int,
    val user:UserDTO,
    val document:StudentDocumentDTO,
    val group:GroupDTO,
    val department:DepartmentDTO,
    @Serializable(with = TrimStringSerializer::class)
    val addedInfo:String = "Отсутсвуют",
)

fun StudentModel.validate(){
    user.validate()
    document.validate()
    group.validate()
    department.validate()
    addedInfo.validate(
        maxLength = StudentConstants.ADDED_INFO_FIELD_SIZE,
        fieldName = StudentConstants.ADDED_INFO_FIELD
    )
}
