package com.example.domain.model.student

import com.example.domain.model.group.GroupConstants
import com.example.domain.model.student_document.StudentDocumentDTO
import com.example.domain.model.user.UserFIO
import com.example.domain.utils.validate
import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class StudentCreateModel(
    val fio:UserFIO,
    val document: StudentDocumentDTO,
    @Serializable(with = TrimStringSerializer::class)
    val addedInfo:String = StudentConstants.DEFAULT_ADDED_INFO,
    @Serializable(with = TrimStringSerializer::class)
    val groupName:String,
)

fun StudentCreateModel.validate(){
    groupName.validate(GroupConstants.NAME_FIELD_SIZE,GroupConstants.NAME_FIELD)
    addedInfo.validate(StudentConstants.ADDED_INFO_FIELD_SIZE,StudentConstants.ADDED_INFO_FIELD)
}