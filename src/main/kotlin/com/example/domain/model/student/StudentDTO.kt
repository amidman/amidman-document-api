package com.example.domain.model.student

import com.example.domain.utils.validate
import kotlinx.serialization.Serializable


@Serializable
data class StudentDTO(
    val id:Int? = null,
    val userId:Int,
    val groupId:Int,
    val studentDocumentId:Int,
    val addedInfo:String = StudentConstants.DEFAULT_ADDED_INFO,
)

fun StudentDTO.validate(){
    addedInfo.validate(StudentConstants.ADDED_INFO_FIELD_SIZE,StudentConstants.ADDED_INFO_FIELD)
}