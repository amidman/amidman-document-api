package com.example.domain.model.student

object StudentConstants {
    const val ADDED_INFO_FIELD = "Статус студента"
    const val ADDED_INFO_FIELD_SIZE = 128
    const val DEFAULT_ADDED_INFO = "Отсутсвует"
}