package com.example.domain.model.student_request

import com.example.domain.model.student.StudentModel
import kotlinx.serialization.Serializable


@Serializable
data class RequestModel(
    val request:StudentRequestDTO,
    val student:StudentModel
)
