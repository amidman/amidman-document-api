package com.example.domain.model.student_request

import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable


@Serializable
data class StudentRequestDTO(
    val id:Int? = null,
    val studentId:Int,
    val status:StudentRequestStatus,
    val type:StudentRequestType,
    val count:Int,
    val date:LocalDate,
    val departmentId:Int,
)

enum class StudentRequestStatus{
    SEND,
    ACCEPTED,
}

enum class StudentRequestType{
    STUDYDOCUMENT,
}
