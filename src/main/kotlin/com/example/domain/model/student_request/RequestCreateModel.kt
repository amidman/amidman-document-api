package com.example.domain.model.student_request

import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable


@Serializable
data class RequestCreateModel(
    val count: Int,
    val date: LocalDate,
    val type: StudentRequestType
)