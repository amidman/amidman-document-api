package com.example.domain.model.group

object GroupConstants {
    const val NAME_FIELD = "Название группы"
    const val NAME_FIELD_SIZE = 32
    private const val MIN_YEAR_COUNT = 1
    private const val MAX_YEAR_COUNT = 4
    private const val MAX_YEAR_COUNT_LOW_LIMIT = 3
    private const val MAX_YEAR_COUNT_LIMIT = 4
    val yearCountRange = (MIN_YEAR_COUNT..MAX_YEAR_COUNT)
    val maxYearCountRange = (MAX_YEAR_COUNT_LOW_LIMIT..MAX_YEAR_COUNT_LIMIT)
}