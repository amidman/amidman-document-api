package com.example.domain.model.group

import com.example.domain.utils.validate
import com.example.errors.entity_errors.GroupErrors
import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class GroupDTO(
    val id: Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val groupName: String,
    val status: GroupStatus,
    val currentYearCount: Int,
    val maxYearCount: Int,
    val departmentId: Int
)

fun GroupDTO.validate(){
    groupName.validate(GroupConstants.NAME_FIELD_SIZE, GroupConstants.NAME_FIELD)
    if (currentYearCount !in GroupConstants.yearCountRange)
        throw GroupErrors.WRONG_GROUP_YEAR_COUNT
    if (maxYearCount !in GroupConstants.maxYearCountRange)
        throw GroupErrors.WRONG_GROUP_MAX_YEAR_COUNT
    if (currentYearCount > maxYearCount)
        throw GroupErrors.WRONG_GROUP_YEAR_COUNT
}

enum class GroupStatus {
    BUDGET,
    NOBUGET
}