package com.example.domain.model.group

import com.example.domain.model.department.DepartmentDTO
import com.example.domain.model.department.validate
import com.example.domain.model.student.StudentDTO
import com.example.domain.model.student.StudentModel
import com.example.domain.model.student.validate
import kotlinx.serialization.Serializable


@Serializable
data class GroupModel(
    val group:GroupDTO,
    val studentList:List<StudentModel>,
    val department:DepartmentDTO
)

fun GroupModel.validate(){
    group.validate()
    department.validate()
    studentList.forEach { studentModel -> studentModel.validate() }
}