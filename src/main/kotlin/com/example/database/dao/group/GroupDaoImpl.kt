package com.example.database.dao.group

import com.example.database.dao.department.DepartmentDaoImpl
import com.example.domain.dao.GroupDao
import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.model.group.GroupConstants
import com.example.domain.model.group.GroupDTO
import com.example.errors.EntityError
import com.example.errors.entity_errors.GroupErrors
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object GroupDaoImpl: BaseDao<GroupDTO>(DaoConstants.groupTableName), GroupDao {

    val groupName = varchar("groupname",GroupConstants.NAME_FIELD_SIZE).uniqueIndex()
    val status = varchar("status",32)
    val currentYearCount = integer("currentyearcount") // сколько лет они учатся в данный момент
    val maxYearCount = integer("maxyearcount") // сколько лет они будут учиться
    val departmentId = reference("departmentid",DepartmentDaoImpl.id)

    override val wrapper: BaseDaoWrapper<GroupDTO>
        get() = GroupDaoWrapper

    override fun insertStatement(dto: GroupDTO): InsertStatement<Number> =
        insert {
            it[groupName] = dto.groupName
            it[status] = dto.status.name
            it[currentYearCount] = dto.currentYearCount
            it[maxYearCount] = dto.maxYearCount
            it[departmentId] = dto.departmentId
        }

    override suspend fun isGroupNameUnique(groupName: String): Boolean = newSuspendedTransaction{
        val group = select {
            this@GroupDaoImpl.groupName eq groupName
        }.singleOrNull()
        group == null
    }

    override suspend fun findByGroupNameAsync(groupName: String): Deferred<GroupDTO?> =
        findSingleAsync {
            this@GroupDaoImpl.groupName eq groupName
        }

    override suspend fun getAllByDepartmentIdAsync(departmentId: Int): Deferred<List<GroupDTO>> =
        findManyAsync(this@GroupDaoImpl.departmentId eq departmentId)

    override suspend fun getAllByDepartmentNameAsync(departmentName: String): Deferred<List<GroupDTO>> = suspendedTransactionAsync{
        (GroupDaoImpl innerJoin DepartmentDaoImpl)
            .select(DepartmentDaoImpl.departmentName eq departmentName)
            .mapNotNull {
                wrapper.fromResultRow(it)
            }
    }

    override suspend fun updateGroupNameAsync(groupId: Int, newGroupName: String): Deferred<Int> =
        updateByIdAsync(
            id = groupId,
            body = {
                it[groupName] = newGroupName
            }
        )

    override suspend fun updateGroupYearCountAsync(groupId: Int, newYearCount: Int): Deferred<Int> =
        updateByIdAsync(
            id = groupId,
            body = {
                it[currentYearCount] = newYearCount
            }
        )

    override suspend fun updateGroupMaxYearCountAsync(groupId: Int, newMaxYearCount: Int): Deferred<Int> =
        updateByIdAsync(
            id = groupId,
            body = {
                it[maxYearCount] = newMaxYearCount
            }
        )


}