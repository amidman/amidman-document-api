package com.example.database.dao.group

import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.model.group.GroupDTO
import com.example.domain.model.group.GroupStatus
import org.jetbrains.exposed.sql.ResultRow

object GroupDaoWrapper:BaseDaoWrapper<GroupDTO> {
    override fun fromResultRow(row: ResultRow): GroupDTO =
        GroupDTO(
            id = row[GroupDaoImpl.id],
            groupName = row[GroupDaoImpl.groupName],
            status = GroupStatus.valueOf(row[GroupDaoImpl.status]),
            currentYearCount = row[GroupDaoImpl.currentYearCount],
            maxYearCount = row[GroupDaoImpl.maxYearCount],
            departmentId = row[GroupDaoImpl.departmentId],
        )

}