package com.example.database.dao.user

import com.example.domain.dao.UserDao
import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.model.user.UserConstants
import com.example.domain.model.user.UserDTO
import com.example.domain.model.user.UserFIO
import com.example.errors.EntityError
import com.example.errors.entity_errors.UserErrors
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

object UserDaoImpl:BaseDao<UserDTO>(DaoConstants.userTableName),UserDao {

    val login = varchar("login",UserConstants.LOGIN_FIELD_SIZE).uniqueIndex()
    val password = varchar("password",UserConstants.PASSWORD_FIELD_SIZE)
    val name = varchar("name",UserConstants.NAME_FIELD_SIZE)
    val surname = varchar("surname",UserConstants.SURNAME_FIELD_SIZE)
    val fatherName = varchar("fathername",UserConstants.FATHER_NAME_FIELD_SIZE)
    val role = varchar("role",32)
    val email = varchar("email",60).nullable().uniqueIndex()

    override val wrapper: BaseDaoWrapper<UserDTO>
        get() = UserDaoMapper

    override fun insertStatement(dto: UserDTO): InsertStatement<Number> =
        insert {insertS ->
            insertS[login] = dto.login
            insertS[password] = dto.password
            insertS[name] = dto.name
            insertS[surname] = dto.surname
            insertS[fatherName] = dto.fatherName
            insertS[role] = dto.role.name
            insertS[email] = dto.email
        }

    override suspend fun updateNameAsync(id:Int,name:String): Deferred<Int> =
        updateByIdAsync(
            id = id,
            body = {
                it[this@UserDaoImpl.name] = name
            }
        )

    override suspend fun updateSurnameAsync(id:Int,surname:String): Deferred<Int> =
        updateByIdAsync(
            id = id,
            body = {
                it[this@UserDaoImpl.surname] = surname
            }
        )
    override suspend fun updateFatherNameAsync(id:Int,fatherName:String): Deferred<Int> =
        updateByIdAsync(
            id = id,
            body = {
                it[this@UserDaoImpl.fatherName] = fatherName
            }
        )
    override suspend fun updateLoginAsync(id:Int,login:String): Deferred<Int> =
        updateByIdAsync(
            id = id,
            body = {
                it[this@UserDaoImpl.login] = login
            }
        )
    override suspend fun updatePasswordAsync(id:Int,password:String): Deferred<Int> =
        updateByIdAsync(
            id = id,
            body = {
                it[this@UserDaoImpl.password] = password
            }
        )

    override suspend fun updateRoleAsync(id: Int, role: String): Deferred<Int> =
        updateByIdAsync(
            id = id,
            body = {
                it[this@UserDaoImpl.role] = role
            }
        )

    override suspend fun updateEmailAsync(id: Int, email: String): Deferred<Int> =
        updateByIdAsync(
            id = id,
            body = {
                it[this@UserDaoImpl.email] = email
            }
        )



    override suspend fun findByLoginAsync(login: String): Deferred<UserDTO?> =
        findSingleAsync {
            this@UserDaoImpl.login eq login
        }

    override suspend fun updateFIOAsync(userId: Int, userFio: UserFIO): Deferred<Int> =
        updateByIdAsync(
            id = userId,
            body = {
                it[name] = userFio.name
                it[surname] = userFio.surname
                it[fatherName] = userFio.fatherName
            }
        )

    override suspend fun isLoginUnique(login: String): Boolean = newSuspendedTransaction {
        val user = select {
            this@UserDaoImpl.login eq login
        }.singleOrNull()
        user == null
    }

    override suspend fun isEmailUnique(email: String): Boolean = newSuspendedTransaction {
        val user = select {
            this@UserDaoImpl.email eq email
        }.singleOrNull()
        user == null
    }


}

