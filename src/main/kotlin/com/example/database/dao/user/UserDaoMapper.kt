package com.example.database.dao.user

import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.model.user.UserDTO
import com.example.security.middleware.ROLES
import org.jetbrains.exposed.sql.ResultRow

object UserDaoMapper: BaseDaoWrapper<UserDTO> {

    override fun fromResultRow(row: ResultRow): UserDTO {
        return UserDTO(
            id = row[UserDaoImpl.id],
            login = row[UserDaoImpl.login],
            password = row[UserDaoImpl.password],
            name = row[UserDaoImpl.name],
            surname = row[UserDaoImpl.surname],
            fatherName = row[UserDaoImpl.fatherName],
            role = ROLES.valueOf(row[UserDaoImpl.role]),
            email = row[UserDaoImpl.email]
        )
    }

}