package com.example.database.dao.department

import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.model.department.DepartmentDTO
import org.jetbrains.exposed.sql.ResultRow

object DepartmentDaoWrapper: BaseDaoWrapper<DepartmentDTO> {

    override fun fromResultRow(row: ResultRow): DepartmentDTO {
        return DepartmentDTO(
            id = row[DepartmentDaoImpl.id],
            departmentName = row[DepartmentDaoImpl.departmentName],
            departmentShortName = row[DepartmentDaoImpl.departmentShortName]
        )
    }
}