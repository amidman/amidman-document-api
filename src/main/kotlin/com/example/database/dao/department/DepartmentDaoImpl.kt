package com.example.database.dao.department

import com.example.domain.dao.DepartmentDao
import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.model.department.DepartmentConstants
import com.example.domain.model.department.DepartmentDTO
import com.example.errors.EntityError
import com.example.errors.entity_errors.DepartmentErrors
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

object DepartmentDaoImpl : BaseDao<DepartmentDTO>(DaoConstants.departmentTableName),DepartmentDao {


    val departmentName = varchar("departmentname", DepartmentConstants.NAME_FIELD_SIZE).uniqueIndex()
    val departmentShortName = varchar("departmentshortname", DepartmentConstants.SHORT_NAME_FIELD_SIZE).default("NN")

    override val wrapper: BaseDaoWrapper<DepartmentDTO>
        get() = DepartmentDaoWrapper

    override fun insertStatement(dto: DepartmentDTO): InsertStatement<Number> =
        insert {
            it[departmentName] = dto.departmentName
            it[departmentShortName] = dto.departmentShortName
        }

    override suspend fun findByDepartmentNameAsync(departmentName: String): Deferred<DepartmentDTO?> =
        findSingleAsync {
            this@DepartmentDaoImpl.departmentName eq departmentName
        }

    override suspend fun isDepartmentNameUnique(departmentName: String): Boolean = newSuspendedTransaction {
        val department = select {
            this@DepartmentDaoImpl.departmentName eq departmentName
        }.singleOrNull()
        department == null
    }

    override suspend fun updateDepartmentNameAsync(departmentId: Int, departmentName: String): Deferred<Int> =
        updateByIdAsync(
            id = departmentId,
            body = {
                it[this@DepartmentDaoImpl.departmentName] = departmentName
            }
        )

    override suspend fun updateShortNameAsync(departmentId: Int, newShortName: String): Deferred<Int> =
        updateByIdAsync(
            id = departmentId,
            body = {
                it[this@DepartmentDaoImpl.departmentShortName] = newShortName
            }
        )

}