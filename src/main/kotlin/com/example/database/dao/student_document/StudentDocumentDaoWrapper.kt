package com.example.database.dao.student_document

import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.model.student_document.StudentDocumentDTO
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.toKotlinLocalDate
import kotlinx.datetime.toKotlinLocalDateTime
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.jetbrains.exposed.sql.ResultRow

object StudentDocumentDaoWrapper:BaseDaoWrapper<StudentDocumentDTO> {
    override fun fromResultRow(row: ResultRow): StudentDocumentDTO {
        return StudentDocumentDTO(
            id = row[StudentDocumentDaoImpl.id],
            docNumber = row[StudentDocumentDaoImpl.docNumber],
            orderNumber = row[StudentDocumentDaoImpl.orderNumber],
            orderDate = row[StudentDocumentDaoImpl.orderDate].toKotlinLocalDate(),
            studyStartDate = row[StudentDocumentDaoImpl.studyStartDate].toKotlinLocalDate(),
        )
    }
}
