package com.example.database.dao.student_document

import com.example.domain.dao.StudentDocumentDao
import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.model.student_document.StudentDocumentConstants
import com.example.domain.model.student_document.StudentDocumentDTO
import com.example.errors.EntityError
import com.example.errors.entity_errors.StudentDocumentErrors
import kotlinx.coroutines.Deferred
import kotlinx.datetime.toJavaLocalDate
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.javatime.date
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

object StudentDocumentDaoImpl: BaseDao<StudentDocumentDTO>(DaoConstants.studentDocumentTableName),StudentDocumentDao {


    val docNumber = varchar("docnumber",StudentDocumentConstants.DOC_NUMBER_FIELD_SIZE).uniqueIndex()
    val orderNumber = varchar("ordernumber",StudentDocumentConstants.ORDER_NUMBER_FIELD_SIZE)
    val orderDate = date("orderdate")
    val studyStartDate = date("studystartdate")


    override val wrapper: BaseDaoWrapper<StudentDocumentDTO>
        get() = StudentDocumentDaoWrapper

    override fun insertStatement(dto: StudentDocumentDTO): InsertStatement<Number> =
        insert {
            it[docNumber] = dto.docNumber
            it[orderNumber] = dto.orderNumber
            it[orderDate] = dto.orderDate.toJavaLocalDate()
            it[studyStartDate] = dto.studyStartDate.toJavaLocalDate()
        }


    override suspend fun changeDocumentNumberAsync(docId: Int, newDocNumber: String): Deferred<Int> =
        updateByIdAsync(
            id = docId,
            body = {
                it[docNumber] = newDocNumber
            }
        )

    override suspend fun changeOrderNumberAsync(docId: Int, newOrderNumber: String): Deferred<Int> =
        updateByIdAsync(
            id = docId,
            body = {
                it[orderNumber] = newOrderNumber
            }
        )

    override suspend fun findByDocNumberAsync(docNumber: String): Deferred<StudentDocumentDTO?> =
        findSingleAsync {
            this@StudentDocumentDaoImpl.docNumber eq docNumber
        }

    override suspend fun findByOrderNumberAsync(orderNumber: String): Deferred<List<StudentDocumentDTO>> =
        findManyAsync {
            this@StudentDocumentDaoImpl.orderNumber eq orderNumber
        }

    override suspend fun isDocNumberUnique(docNumber: String): Boolean = newSuspendedTransaction {
        val document = select {
            this@StudentDocumentDaoImpl.docNumber eq docNumber
        }.singleOrNull()
        document == null
    }


}