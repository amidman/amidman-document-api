package com.example.database.dao.student

import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.model.student.StudentDTO
import org.jetbrains.exposed.sql.ResultRow

object StudentDaoWrapper: BaseDaoWrapper<StudentDTO> {
    override fun fromResultRow(row: ResultRow): StudentDTO {
        return StudentDTO(
            id = row[StudentDaoImpl.id],
            userId = row[StudentDaoImpl.userId],
            groupId = row[StudentDaoImpl.groupId],
            studentDocumentId = row[StudentDaoImpl.studentDocumentId],
            addedInfo = row[StudentDaoImpl.addedInfo],
        )
    }
}