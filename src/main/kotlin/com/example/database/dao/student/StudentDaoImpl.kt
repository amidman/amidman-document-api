package com.example.database.dao.student

import com.example.database.dao.department.DepartmentDaoImpl
import com.example.database.dao.department.DepartmentDaoWrapper
import com.example.database.dao.group.GroupDaoImpl
import com.example.database.dao.group.GroupDaoWrapper
import com.example.database.dao.student_document.StudentDocumentDaoWrapper
import com.example.database.dao.student_document.StudentDocumentDaoImpl
import com.example.database.dao.user.UserDaoMapper
import com.example.database.dao.user.UserDaoImpl
import com.example.domain.dao.StudentDao
import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.model.student.StudentConstants
import com.example.domain.model.student.StudentDTO
import com.example.domain.model.student.StudentModel
import com.example.errors.EntityError
import com.example.errors.entity_errors.StudentErrors
import kotlinx.coroutines.Deferred
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object StudentDaoImpl: BaseDao<StudentDTO>(DaoConstants.studentTableName),StudentDao {

    val userId = reference("userid",UserDaoImpl.id,onDelete = ReferenceOption.CASCADE).uniqueIndex()
    val groupId = reference("groupid",GroupDaoImpl.id, onDelete = ReferenceOption.CASCADE)
    val studentDocumentId = reference("studentdocumentid",StudentDocumentDaoImpl.id, onDelete = ReferenceOption.CASCADE).uniqueIndex()
    val addedInfo = varchar("addedinfo",StudentConstants.ADDED_INFO_FIELD_SIZE).default(StudentConstants.DEFAULT_ADDED_INFO)


    override val wrapper: BaseDaoWrapper<StudentDTO>
        get() = StudentDaoWrapper

    override fun insertStatement(dto: StudentDTO): InsertStatement<Number> =
        insert {
            it[userId] = dto.userId
            it[groupId] = dto.groupId
            it[studentDocumentId] = dto.studentDocumentId
            it[addedInfo] = dto.addedInfo
        }

    override suspend fun findByUserIdAsync(userId: Int): Deferred<StudentDTO?> =
        findSingleAsync {
            this@StudentDaoImpl.userId eq userId
        }

    private suspend fun findManyStudentModel(body: Op<Boolean>) = suspendedTransactionAsync {
        (StudentDaoImpl innerJoin GroupDaoImpl innerJoin UserDaoImpl innerJoin StudentDocumentDaoImpl innerJoin DepartmentDaoImpl)
            .select(body)
            .mapNotNull {
                StudentModel(
                    id = it[this@StudentDaoImpl.id],
                    user = UserDaoMapper.fromResultRow(it),
                    group = GroupDaoWrapper.fromResultRow(it),
                    document = StudentDocumentDaoWrapper.fromResultRow(it),
                    department = DepartmentDaoWrapper.fromResultRow(it),
                    addedInfo = it[this@StudentDaoImpl.addedInfo]
                )
            }
    }

    private suspend fun findSingleStudentModel(body:Op<Boolean>) = suspendedTransactionAsync {
        (StudentDaoImpl innerJoin GroupDaoImpl innerJoin UserDaoImpl innerJoin StudentDocumentDaoImpl innerJoin DepartmentDaoImpl)
            .select(body)
            .mapNotNull {
                StudentModel(
                    id = it[this@StudentDaoImpl.id],
                    user = UserDaoMapper.fromResultRow(it),
                    group = GroupDaoWrapper.fromResultRow(it),
                    document = StudentDocumentDaoWrapper.fromResultRow(it),
                    department = DepartmentDaoWrapper.fromResultRow(it),
                    addedInfo = it[this@StudentDaoImpl.addedInfo]
                )
            }.singleOrNull()
    }

    override suspend fun findStudentModelAsync(studentId: Int): Deferred<StudentModel?> =
        findSingleStudentModel(this@StudentDaoImpl.id eq studentId)

    override suspend fun getAllStudentModelAsync(): Deferred<List<StudentModel>> = suspendedTransactionAsync{
        (StudentDaoImpl innerJoin GroupDaoImpl innerJoin UserDaoImpl innerJoin StudentDocumentDaoImpl innerJoin DepartmentDaoImpl)
            .selectAll()
            .mapNotNull {
                StudentModel(
                    id = it[this@StudentDaoImpl.id],
                    user = UserDaoMapper.fromResultRow(it),
                    group = GroupDaoWrapper.fromResultRow(it),
                    document = StudentDocumentDaoWrapper.fromResultRow(it),
                    department = DepartmentDaoWrapper.fromResultRow(it),
                    addedInfo = it[this@StudentDaoImpl.addedInfo]
                )
            }
    }

    override suspend fun getAllStudentModelByGroupAsync(groupId: Int): Deferred<List<StudentModel>> =
        findManyStudentModel(this@StudentDaoImpl.groupId eq groupId)

    override suspend fun getAllStudentModelByGroupNameAsync(groupName: String): Deferred<List<StudentModel>> =
        findManyStudentModel(GroupDaoImpl.groupName eq groupName)

    override suspend fun getAllStudentModelByDepartmentAsync(departmentId: Int): Deferred<List<StudentModel>> =
        findManyStudentModel(GroupDaoImpl.departmentId eq departmentId)

    override suspend fun updateGroupAsync(studentId: Int, groupId: Int): Deferred<Int> =
        updateByIdAsync(
            id = studentId,
            body = {
                it[this@StudentDaoImpl.groupId] = groupId
            }
        )

    override suspend fun updateAddedInfoAsync(studentId: Int, addedInfo: String): Deferred<Int> =
        updateByIdAsync(
            id = studentId,
            body = {
                it[this@StudentDaoImpl.addedInfo] = addedInfo
            }
        )


}