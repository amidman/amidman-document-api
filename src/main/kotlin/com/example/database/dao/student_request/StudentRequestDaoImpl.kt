package com.example.database.dao.student_request

import com.example.database.dao.department.DepartmentDaoImpl
import com.example.database.dao.student.StudentDaoImpl
import com.example.domain.dao.StudentRequestDao
import com.example.domain.dao.basic.BaseDao
import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.dao.basic.DaoConstants
import com.example.domain.model.student_request.StudentRequestDTO
import com.example.domain.model.student_request.StudentRequestStatus
import com.example.errors.EntityError
import com.example.errors.entity_errors.StudentRequestErrors
import kotlinx.coroutines.Deferred
import kotlinx.datetime.toJavaLocalDate
import org.jetbrains.exposed.sql.javatime.date
import org.jetbrains.exposed.sql.statements.InsertStatement
import kotlinx.datetime.LocalDate
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync

object StudentRequestDaoImpl : BaseDao<StudentRequestDTO>(DaoConstants.studentRequestTableName),StudentRequestDao {

    val studentId = reference("studentid", StudentDaoImpl.id, onDelete = ReferenceOption.CASCADE)
    val status = varchar("status", 16)
    val type = varchar("type", 16)
    val departmentId = reference("departmentid", DepartmentDaoImpl.id)
    val count = integer("countvalue").default(1)
    val date = date("dateofrequest").default(java.time.LocalDate.now())

    override val wrapper: BaseDaoWrapper<StudentRequestDTO>
        get() = StudentRequestDaoWrapper

    override fun insertStatement(dto: StudentRequestDTO): InsertStatement<Number> =
        insert {
            it[studentId] = dto.studentId
            it[status] = dto.status.name
            it[type] = dto.type.name
            it[departmentId] = dto.departmentId
            it[count] = dto.count
            it[date] = dto.date.toJavaLocalDate()
        }

    override suspend fun updateRequestStatusAsync(
        requestId: Int,
        newRequestStatus: StudentRequestStatus
    ): Deferred<Int> = updateByIdAsync(
        id = requestId,
        body = {
            it[status] = newRequestStatus.name
        }
    )

    override suspend fun getByDepartmentIdAsync(departmentId: Int):
            Deferred<List<StudentRequestDTO>> =
        findManyAsync {
            this@StudentRequestDaoImpl.departmentId eq departmentId
        }

    override suspend fun getAllNotAcceptedByDepartmentIdAsync(departmentId: Int):
            Deferred<List<StudentRequestDTO>> =
        findManyAsync {
            not(this@StudentRequestDaoImpl.status eq StudentRequestStatus.ACCEPTED.name) and
                    (this@StudentRequestDaoImpl.departmentId eq departmentId)
        }

    override suspend fun getByStudentIdAsync(studentId: Int): Deferred<List<StudentRequestDTO>> = findManyAsync {
        this@StudentRequestDaoImpl.studentId eq studentId
    }

    override suspend fun getAcceptedListInDateRangeAsync(
        departmentId: Int,
        startDate: LocalDate,
        endDate: LocalDate,
    ): Deferred<List<StudentRequestDTO>> = findManyAsync {
        date greaterEq startDate.toJavaLocalDate() and
                (date lessEq endDate.toJavaLocalDate()) and
                (this@StudentRequestDaoImpl.departmentId eq departmentId) and
                (this@StudentRequestDaoImpl.status eq StudentRequestStatus.ACCEPTED.name)
    }

    override suspend fun getLastNRequestsAsync(departmentId: Int, count: Int,offset:Long):
            Deferred<List<StudentRequestDTO>> =
        findManyAsync(
            n = count,
            offset = offset,
            where =
                this@StudentRequestDaoImpl.departmentId eq departmentId and
                        (status eq StudentRequestStatus.ACCEPTED.name)
        )


}