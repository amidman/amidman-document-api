package com.example.database.dao.student_request

import com.example.domain.dao.basic.BaseDaoWrapper
import com.example.domain.model.student_request.StudentRequestDTO
import com.example.domain.model.student_request.StudentRequestStatus
import com.example.domain.model.student_request.StudentRequestType
import kotlinx.datetime.toKotlinLocalDate
import org.jetbrains.exposed.sql.ResultRow

object StudentRequestDaoWrapper:BaseDaoWrapper<StudentRequestDTO> {
    override fun fromResultRow(row: ResultRow): StudentRequestDTO {
        return StudentRequestDTO(
            id = row[StudentRequestDaoImpl.id],
            studentId = row[StudentRequestDaoImpl.studentId],
            type = StudentRequestType.valueOf(row[StudentRequestDaoImpl.type]),
            status = StudentRequestStatus.valueOf(row[StudentRequestDaoImpl.status]),
            departmentId = row[StudentRequestDaoImpl.departmentId],
            date = row[StudentRequestDaoImpl.date].toKotlinLocalDate(),
            count = row[StudentRequestDaoImpl.count],
        )
    }
}