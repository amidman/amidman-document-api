package com.example.database


import com.example.domain.model.config.DatabaseConfig
import io.ktor.server.application.*
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.koin.ktor.ext.inject

fun Application.configureDataBase() {
    val config by inject<DatabaseConfig>()


    Database.connect(
        url = config.dbUrl,
        user = config.user,
        password = config.password,
        driver = config.driverName
    )
    val flyway = Flyway.configure()
        .dataSource(config.dbUrl, config.user, config.password)
        .defaultSchema("public")
        .baselineOnMigrate(true)
        .load()
    flyway.migrate()
}