package com.example.plugins

import com.example.domain.model.config.ApplicationConfigData
import com.example.errors.utils.ErrorData
import io.github.smiley4.ktorswaggerui.SwaggerUI
import io.github.smiley4.ktorswaggerui.dsl.AuthScheme
import io.github.smiley4.ktorswaggerui.dsl.AuthType
import io.github.smiley4.ktorswaggerui.dsl.OpenApiRoute
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import org.koin.ktor.ext.inject
import kotlin.reflect.KClass


fun OpenApiRoute.requestBody(type: KClass<*>){
    request { body(type) }
}

fun OpenApiRoute.responseBody(type: KClass<*>) {
    response {
        HttpStatusCode.OK to { body(type) }
        HttpStatusCode.NotFound to { body(ErrorData::class) }
    }
}

fun basicSwaggerBuilder(
    description:String? = null,
    requestBody:KClass<*>? = null,
    responseBody:KClass<*>? = null,
):OpenApiRoute.() -> Unit = {
    this.description = description
    requestBody?.let { requestBody(requestBody) }
    responseBody?.let { responseBody(responseBody) }
}

fun tagBuilder(tag:String):OpenApiRoute.() -> Unit = {
    tags = listOf(tag)
}

inline fun <reified T> OpenApiRoute.queryParam(name:String){
    request {
        queryParameter<T>(name)
    }
}

fun Application.configureSwaggerUI(){
    val config by inject<ApplicationConfigData>()
    install(SwaggerUI){
        swagger {
            swaggerUrl = "swagger-ui"
            forwardRoot = true
        }
        info {
            title = "Amid Document API"
            version = "latest"
            description = "API for document work aromatization"
        }
        server {
            url = "${config.swagger_host}:${config.port}"
            description = "Development Server"
        }
        securityScheme("Authorization") {
            type = AuthType.HTTP
            scheme = AuthScheme.BEARER
            bearerFormat = "jwt"
        }
        defaultSecuritySchemeName = "Authorization"
        schemasInComponentSection = true

    }
}