package com.example.plugins

import com.auth0.jwt.exceptions.JWTDecodeException
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.application.*

import com.auth0.jwt.exceptions.TokenExpiredException
import com.example.errors.entity_errors.BasicErrors
import com.example.errors.middleware_errors.TokenErrors
import com.example.errors.utils.ErrorData
import com.example.errors.utils.ResponseException
import io.ktor.http.*
import io.ktor.server.plugins.*
import io.ktor.server.plugins.requestvalidation.*
import io.ktor.server.response.*
import org.jetbrains.exposed.exceptions.ExposedSQLException
import java.time.format.DateTimeParseException

fun Application.configureStatusPages() {

    install(StatusPages) {

        exception<ResponseException> { call, rExc ->
            call.respond(message = rExc.errorData, status = rExc.statusCode)
        }

        exception<ExposedSQLException> { call, exposedSQLException ->
            val errorData = ErrorData(
                code = "sql error",
                description = "Ошибка базы данных"
            )
            call.respond(message = errorData, status = HttpStatusCode.NotFound)
        }

        exception<TokenExpiredException> { call, tokenExpiredException ->
            val errorData = ErrorData(
                code = "token expired",
                description = "Время токена вышло"
            )
            call.respond(message = errorData, status = HttpStatusCode.Unauthorized)
        }

        exception<RequestValidationException> { call, requestValidationException ->
            val errorData = ErrorData(
                code = "validation failed",
                description = requestValidationException.reasons.joinToString()
            )
            call.respond(message = errorData, status = HttpStatusCode.BadRequest)
        }

        exception<IllegalArgumentException> { call, illegalArgumentException ->
            val errorData = ErrorData(
                code = "illegal argument",
                description = illegalArgumentException.message.toString()
            )
            call.respond(message = errorData, status = HttpStatusCode.BadRequest)

        }

        exception<DateTimeParseException>{call, dateTimeParseException ->
            val errorData = BasicErrors.WRONG_DATE_FORMAT.errorData
            val status = BasicErrors.WRONG_DATE_FORMAT.statusCode
            call.respond(message = errorData, status = status)
        }

        exception<BadRequestException>{call, badRequestException ->
            val exception = if (badRequestException.cause is ResponseException)
                badRequestException.cause as ResponseException
            else
                BasicErrors.BAD_REQUEST
            val errorData = exception.errorData
            val statusCode = exception.statusCode
            call.respond(message = errorData, status = statusCode)
        }

        exception<JWTDecodeException>{call, jwtDecodeException ->
            val errorData = TokenErrors.INVALID_TOKEN
            call.respond(message = errorData.errorData,status = errorData.statusCode)
        }




    }
}
