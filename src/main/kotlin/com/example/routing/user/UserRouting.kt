package com.example.routing.user

import com.example.domain.controller.UserController
import com.example.domain.model.auth.LoginData
import com.example.domain.model.auth.TokenBody
import com.example.domain.model.user.UserDTO
import com.example.domain.model.user.UserFIO
import com.example.plugins.basicSwaggerBuilder
import com.example.plugins.queryParam
import com.example.plugins.requestBody
import com.example.routing.getIntParam
import com.example.routing.getQueryParam
import com.example.routing.getUserId
import com.example.security.middleware.ROLES
import io.github.smiley4.ktorswaggerui.dsl.delete
import io.github.smiley4.ktorswaggerui.dsl.get
import io.github.smiley4.ktorswaggerui.dsl.post
import io.github.smiley4.ktorswaggerui.dsl.put
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject

fun Route.userRouting() {
    val userController by inject<UserController>()

    authenticate(ROLES.ADMIN.name){
        post(
            UserRoutingConstants.REGISTER,
            basicSwaggerBuilder(
                description = "for admin",
                requestBody = UserDTO::class,
                responseBody = UserDTO::class
            )
        ) {
            val user = call.receive<UserDTO>()
            val userDTO = userController.registerUser(user)
            call.respond(userDTO)
        }

        delete(UserRoutingConstants.DELETE,
            {
                queryParam<Int>(UserRoutingConstants.userIdQ)
            }
        ) {
            val id = call.getIntParam(UserRoutingConstants.userIdQ)
            userController.delete(id)
            call.respond(HttpStatusCode.NoContent)
        }

        put(UserRoutingConstants.CHANGE_FIO,
            {
                description = "for admin, change user fio by id"
                queryParam<Int>(UserRoutingConstants.userIdQ)
                requestBody(UserFIO::class)
            }
        ){
            val userId = call.getIntParam(UserRoutingConstants.userIdQ)
            val userFio = call.receive<UserFIO>()
            userController.changeFio(userId,userFio)
            call.respond(HttpStatusCode.NoContent)
        }

        put(UserRoutingConstants.CHANGE_LOGIN,
            {
                description = "for admin, login length max 32 characters"
                queryParam<Int>(UserRoutingConstants.userIdQ)
                queryParam<String>(UserRoutingConstants.loginQ)
            }
        ){
            val userId = call.getIntParam(UserRoutingConstants.userIdQ)
            val login = call.getQueryParam(UserRoutingConstants.loginQ,32)
            userController.changeLogin(userId,login)
            call.respond(HttpStatusCode.NoContent)
        }

        put(UserRoutingConstants.CHANGE_PASSWORD,
            {
                description = "for admin, password length max 32 characters"
                queryParam<Int>(UserRoutingConstants.userIdQ)
                queryParam<String>(UserRoutingConstants.passwordQ)
            }
        ){
            val userId = call.getIntParam(UserRoutingConstants.userIdQ)
            val password = call.getQueryParam(UserRoutingConstants.passwordQ,32)
            userController.changePassword(userId,password)
            call.respond(HttpStatusCode.NoContent)
        }


    }

    authenticate {

        get(UserRoutingConstants.INFO,
            basicSwaggerBuilder(
                responseBody = UserDTO::class
            )
        ){
            val userId = call.getUserId()
            val userDTO = userController.findById(userId)
            call.respond(userDTO)
        }

        get(UserRoutingConstants.ALL,
            basicSwaggerBuilder(
                description = "for admin",
                responseBody = Array<UserDTO>::class
            )
        ) {
            val userDTOList = userController.getAll()
            call.respond(userDTOList)
        }

    }



    post(UserRoutingConstants.LOGIN,
        basicSwaggerBuilder(
            requestBody = LoginData::class,
            responseBody = TokenBody::class
        )
    ) {
        val loginData = call.receive<LoginData>()
        val response = userController.auth(loginData)
        call.respond(response)
    }


}