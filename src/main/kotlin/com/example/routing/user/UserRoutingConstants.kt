package com.example.routing.user

object UserRoutingConstants {

    const val userIdQ = "userId"
    const val loginQ = "login"
    const val passwordQ = "password"

    const val REGISTER = "register"
    const val DELETE = "delete"
    const val LOGIN = "login"
    const val ALL = "all"
    const val INFO = "info"

    const val CHANGE_FIO = "change-fio"
    const val CHANGE_LOGIN = "change-login"
    const val CHANGE_PASSWORD = "change-password"
}