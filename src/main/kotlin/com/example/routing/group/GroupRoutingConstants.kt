package com.example.routing.group



object GroupRoutingConstants {

    const val groupIdQ = "groupId"
    const val groupNameQ = "groupName"
    const val groupYearCountQ = "groupYearCount"
    const val groupMaxYearCountQ = "groupMaxYearCountQ"

    const val CREATE = "create"
    const val DELETE = "delete"
    const val ALL = "all"
    const val CHANGE_GROUP_NAME = "change-group-name"
    const val CHANGE_GROUP_YEAR_COUNT = "change-group-year-count"
    const val CHANGE_GROUP_MAX_YEAR_COUNT = "change-group-max-year-count"
    const val GET_BY_ID = "get-by-id"
    const val GET_BY_NAME = "get-by-name"
}