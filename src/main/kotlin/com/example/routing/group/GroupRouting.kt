package com.example.routing.group

import com.example.domain.controller.GroupController
import com.example.domain.model.group.GroupDTO
import com.example.domain.model.group.GroupModel
import com.example.plugins.basicSwaggerBuilder
import com.example.plugins.queryParam
import com.example.plugins.responseBody
import com.example.routing.getIntParam
import com.example.routing.getQueryParam
import com.example.security.middleware.ROLES
import io.github.smiley4.ktorswaggerui.dsl.delete
import io.github.smiley4.ktorswaggerui.dsl.get
import io.github.smiley4.ktorswaggerui.dsl.post
import io.github.smiley4.ktorswaggerui.dsl.put
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject


fun Route.groupRouting(){

    val groupController by inject<GroupController>()

    authenticate(ROLES.ADMIN.name){

        post(GroupRoutingConstants.CREATE,
            basicSwaggerBuilder(
                description = "for admin",
                requestBody = GroupDTO::class,
                responseBody = GroupDTO::class,
            )
        ) {
            val group = call.receive<GroupDTO>()
            val groupDTO = groupController.createGroup(group)
            call.respond(groupDTO)
        }

        delete(GroupRoutingConstants.DELETE,
            {
                description = "for admin"
                queryParam<Int>(GroupRoutingConstants.groupIdQ)
            }
        ){
            val groupId = call.getIntParam(GroupRoutingConstants.groupIdQ)
            groupController.delete(groupId)
            call.respond(HttpStatusCode.NoContent)
        }

        put(GroupRoutingConstants.CHANGE_GROUP_NAME,
            {
                description = "for admin"
                queryParam<Int>(GroupRoutingConstants.groupIdQ)
                queryParam<String>(GroupRoutingConstants.groupNameQ)
            }
        ) {
            val groupId = call.getIntParam(GroupRoutingConstants.groupIdQ).toInt()
            val groupName = call.getQueryParam(GroupRoutingConstants.groupNameQ,32)
            groupController.changeName(groupId,groupName)
            call.respond(HttpStatusCode.NoContent)
        }

        put(GroupRoutingConstants.CHANGE_GROUP_YEAR_COUNT,
            {
                description = "for admin"
                queryParam<Int>(GroupRoutingConstants.groupIdQ)
                queryParam<Int>(GroupRoutingConstants.groupYearCountQ)
            }
        ) {
            val groupId = call.getIntParam(GroupRoutingConstants.groupIdQ)
            val yearCount = call.getIntParam(GroupRoutingConstants.groupYearCountQ)
            groupController.changeYearCount(groupId = groupId, newYearCount = yearCount)
            call.respond(HttpStatusCode.NoContent)
        }

        put(GroupRoutingConstants.CHANGE_GROUP_MAX_YEAR_COUNT,
            {
                description = "for admin"
                queryParam<Int>(GroupRoutingConstants.groupIdQ)
                queryParam<Int>(GroupRoutingConstants.groupMaxYearCountQ)
            }
        ) {
            val groupId = call.getIntParam(GroupRoutingConstants.groupIdQ)
            val maxYearCount = call.getIntParam(GroupRoutingConstants.groupMaxYearCountQ)
            groupController.changeMaxYearCount(groupId = groupId, maxYearCount = maxYearCount)
            call.respond(HttpStatusCode.NoContent)
        }


    }

    authenticate {
        get(GroupRoutingConstants.GET_BY_ID,
            {
                description = "with token"
                queryParam<Int>(GroupRoutingConstants.groupIdQ)
                responseBody(GroupModel::class)
            }
        ){
            val groupId = call.getIntParam(GroupRoutingConstants.groupIdQ)
            val groupModel = groupController.findGroupModelById(groupId)
            call.respond(groupModel)
        }

        get(GroupRoutingConstants.GET_BY_NAME,
            {
                description = "with token"
                queryParam<String>(GroupRoutingConstants.groupNameQ)
                responseBody(GroupModel::class)
            }
        ){
            val groupName = call.getQueryParam(GroupRoutingConstants.groupNameQ,32)
            val groupModel = groupController.findByGroupModelByName(groupName)
            call.respond(groupModel)
        }

        get(GroupRoutingConstants.ALL,
            {
                description = "with token"
                responseBody(Array<GroupDTO>::class)
            }
        ){
            val groupDTOList = groupController.getAll()
            call.respond(groupDTOList)
        }
    }
}