package com.example.routing.student_request

import com.example.domain.controller.StudentRequestController
import com.example.domain.model.student_request.RequestCreateModel
import com.example.domain.model.student_request.RequestModel
import com.example.domain.model.student_request.StudentRequestDTO
import com.example.plugins.basicSwaggerBuilder
import com.example.plugins.queryParam
import com.example.plugins.responseBody
import com.example.routing.getDateParam
import com.example.routing.getIntParam
import com.example.routing.getStudentId
import com.example.security.middleware.ROLES
import io.github.smiley4.ktorswaggerui.dsl.delete
import io.github.smiley4.ktorswaggerui.dsl.get
import io.github.smiley4.ktorswaggerui.dsl.post
import io.github.smiley4.ktorswaggerui.dsl.put
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject


fun Route.studentRequestRouting(){

    val studentRequestController by inject<StudentRequestController>()

    authenticate(ROLES.STUDENT.name) {

        get(StudentRequestRoutingConstants.GET_BY_STUDENT,
            basicSwaggerBuilder(
                description = "for student",
                responseBody = Array<StudentRequestDTO>::class
            )
        ){
            val studentId = call.getStudentId()
            val requestList = studentRequestController.getRequestListByStudent(studentId)
            call.respond(requestList)
        }

        post(StudentRequestRoutingConstants.CREATE,
            basicSwaggerBuilder(
                description = "for student",
                requestBody = RequestCreateModel::class,
                responseBody = StudentRequestDTO::class,
            )
        ){
            val studentId = call.getStudentId()
            val request = call.receive<RequestCreateModel>()
            val requestDTO = studentRequestController.createStudentRequest(studentId,request)
            call.respond(requestDTO)
        }

        delete(StudentRequestRoutingConstants.DECLINE,
            {
                description = "for student"
                queryParam<Int>(StudentRequestRoutingConstants.studentRequestIdQ)
            }
        ) {
            val requestId = call.getIntParam(StudentRequestRoutingConstants.studentRequestIdQ)
            studentRequestController.delete(requestId)
            call.respond(HttpStatusCode.NoContent)
        }

    }

    authenticate(ROLES.SECRETARY.name) {

        get(StudentRequestRoutingConstants.GET_BY_DEPARTMENT,
            {
                description = "for secretary"
                queryParam<Int>(StudentRequestRoutingConstants.departmentIdQ)
                responseBody(Array<RequestModel>::class)
            }
        ) {
            val departmentId = call.getIntParam(StudentRequestRoutingConstants.departmentIdQ)
            val requestModelList = studentRequestController.getRequestListByDepartment(departmentId)
            call.respond(requestModelList)
        }

        get(StudentRequestRoutingConstants.RECENT,
            {
                description = "for secretary"
                queryParam<Int>(StudentRequestRoutingConstants.departmentIdQ)
                responseBody(Array<RequestModel>::class)
            }
        ) {
            val departmentId = call.getIntParam(StudentRequestRoutingConstants.departmentIdQ)
            val requestModelList = studentRequestController.getAllNotAcceptedByDepartment(departmentId)
            call.respond(requestModelList)
        }

        get(StudentRequestRoutingConstants.HISTORY,
            {
                description = "for secretary"
                queryParam<Int>(StudentRequestRoutingConstants.departmentIdQ)
                queryParam<String>(StudentRequestRoutingConstants.startDateQ)
                queryParam<String>(StudentRequestRoutingConstants.endDateQ)
                responseBody(Array<RequestModel>::class)
            }
        ) {
            val departmentId = call.getIntParam(StudentRequestRoutingConstants.departmentIdQ)
            val startDate = call.getDateParam(StudentRequestRoutingConstants.startDateQ)
            val endDate = call.getDateParam(StudentRequestRoutingConstants.endDateQ)
            val requestModelList = studentRequestController.getAllAcceptedByDate(
                departmentId = departmentId,
                startDate = startDate,
                endDate = endDate,
            )
            call.respond(requestModelList)
        }

        put(StudentRequestRoutingConstants.ACCEPT,
            {
                description = "for secretary"
                queryParam<Int>(StudentRequestRoutingConstants.studentRequestIdQ)
            }
        ) {
            val requestId = call.getIntParam(StudentRequestRoutingConstants.studentRequestIdQ)
            studentRequestController.acceptRequest(requestId)
            call.respond(HttpStatusCode.NoContent)
        }

        get(StudentRequestRoutingConstants.DOCUMENT,
            {
                description = "for secretary, date in 'YYYY-MM-DD' format"
                queryParam<Int>(StudentRequestRoutingConstants.studentRequestIdQ)
                queryParam<String>(StudentRequestRoutingConstants.currentDateQ)
            }
        ){
            val requestId = call.getIntParam(StudentRequestRoutingConstants.studentRequestIdQ)
            val currentDate = call.getDateParam(StudentRequestRoutingConstants.currentDateQ)
            val document = studentRequestController.getDocumentFromRequest(requestId,currentDate)
            call.respondOutputStream {
                document.save(this)
            }
        }

    }
}