package com.example.routing.student_request

object StudentRequestRoutingConstants {

    const val departmentIdQ = "departmentId"
    const val studentRequestIdQ = "studentRequestId"
    const val studentIdQ = "studentId"
    const val currentDateQ = "currentDate"
    const val startDateQ = "startDate"
    const val endDateQ = "endDate"
    const val CREATE = "create"
    const val DECLINE = "decline"
    const val ACCEPT = "accept"
    const val DOCUMENT = "download-document"
    const val GET_BY_DEPARTMENT = "get-by-department"
    const val RECENT = "recent"
    const val HISTORY = "history"
    const val GET_BY_STUDENT = "get-by-student"
}