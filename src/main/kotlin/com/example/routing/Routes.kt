package com.example.routing

object Routes {

    const val USER_ROUTE = "users"
    const val STUDENT_ROUTE = "students"
    const val GROUP_ROUTE = "groups"
    const val DEPARTMENT_ROUTE = "department"
    const val STUDENT_REQUEST_ROUTE = "studentRequest"
}