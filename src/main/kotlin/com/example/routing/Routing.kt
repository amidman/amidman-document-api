package com.example.routing


import com.example.errors.entity_errors.BasicErrors
import com.example.errors.middleware_errors.TokenErrors
import com.example.plugins.tagBuilder
import com.example.routing.department.departmentRouting
import com.example.routing.group.groupRouting
import com.example.routing.student.studentRouting
import com.example.routing.student_request.studentRequestRouting
import com.example.routing.user.userRouting
import com.example.security.middleware.Claims
import com.example.security.middleware.ROLES
import io.github.smiley4.ktorswaggerui.dsl.route
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.routing.*
import kotlinx.datetime.LocalDate


fun ApplicationCall.getUserId(): Int {
    val principal = principal<JWTPrincipal>()
    return principal?.payload?.getClaim(Claims.ID)?.asInt() ?: throw TokenErrors.INVALID_TOKEN
}

fun ApplicationCall.getUserRole(): ROLES {
    val principal = principal<JWTPrincipal>()
    val role = principal?.payload?.getClaim(Claims.ROLE)?.asString() ?: throw TokenErrors.INVALID_TOKEN
    return ROLES.valueOf(role)
}

fun ApplicationCall.getStudentId(): Int {
    val principal = principal<JWTPrincipal>()
    return principal?.payload?.getClaim(Claims.STUDENT_ID)?.asInt() ?: throw TokenErrors.INVALID_TOKEN
}

fun ApplicationCall.getIntParam(paramName: String): Int {
    return parameters[paramName]?.toInt() ?: throw BasicErrors.BAD_REQUEST
}

fun ApplicationCall.getDateParam(paramName: String): LocalDate {
    val stringDate = parameters[paramName]
    stringDate ?: throw BasicErrors.BAD_REQUEST
    return LocalDate.parse(stringDate)
}

fun ApplicationCall.getQueryParam(paramName: String, maxLength: Int): String {
    val paramValue = request.queryParameters[paramName]?.trim() ?: throw BasicErrors.BAD_REQUEST
    if (paramValue.isEmpty())
        throw BasicErrors.EMPTY_FIELDS
    if (paramValue.length > maxLength)
        throw BasicErrors.MANY_SYMBOLS(paramName, maxLength)
    return paramValue
}


fun Application.configureRouting() {

    routing {
        route(path = Routes.USER_ROUTE, builder = tagBuilder(Routes.USER_ROUTE)) { userRouting() }
        route(path = Routes.STUDENT_ROUTE, builder = tagBuilder(Routes.STUDENT_ROUTE)) { studentRouting() }
        route(path = Routes.GROUP_ROUTE, builder = tagBuilder(Routes.GROUP_ROUTE)) { groupRouting() }
        route(path = Routes.DEPARTMENT_ROUTE, builder = tagBuilder(Routes.DEPARTMENT_ROUTE)) { departmentRouting() }
        route(
            path = Routes.STUDENT_REQUEST_ROUTE,
            builder = tagBuilder(Routes.STUDENT_REQUEST_ROUTE)
        ) { studentRequestRouting() }
    }

}
