package com.example.routing.student

import com.example.domain.controller.StudentController
import com.example.domain.model.student.StudentCreateModel
import com.example.domain.model.student.StudentModel
import com.example.plugins.basicSwaggerBuilder
import com.example.plugins.queryParam
import com.example.plugins.responseBody
import com.example.routing.getIntParam
import com.example.routing.getQueryParam
import com.example.routing.getStudentId
import com.example.security.middleware.ROLES
import io.github.smiley4.ktorswaggerui.dsl.delete
import io.github.smiley4.ktorswaggerui.dsl.get
import io.github.smiley4.ktorswaggerui.dsl.post
import io.github.smiley4.ktorswaggerui.dsl.put
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject

fun Route.studentRouting(){
    val studentController by inject<StudentController>()

    authenticate(ROLES.ADMIN.name) {

        post(StudentRoutingConstants.CREATE,
            basicSwaggerBuilder(
                description = "for admin",
                requestBody = StudentCreateModel::class,
                responseBody = StudentModel::class
            )
        ) {
            val studentCreateModel = call.receive<StudentCreateModel>()
            val studentModel = studentController.createStudent(studentCreateModel)
            call.respond(studentModel)
        }

        delete(StudentRoutingConstants.DELETE,
        {
            description = "for admin"
            queryParam<Int>(StudentRoutingConstants.studentIdQ)
        }
        ) {
            val id = call.getIntParam(StudentRoutingConstants.studentIdQ)
            studentController.delete(id)
            call.respond(HttpStatusCode.NoContent)
        }

        put(StudentRoutingConstants.CHANGE_GROUP,
            {
                description = "for admin"
                queryParam<Int>(StudentRoutingConstants.groupIdQ)
                queryParam<Int>(StudentRoutingConstants.studentIdQ)
            }
        ){
            val studentId = call.getIntParam(StudentRoutingConstants.studentIdQ)
            val groupId = call.getIntParam(StudentRoutingConstants.groupIdQ)
            studentController.changeGroup(studentId = studentId,groupId = groupId)
            call.respond(HttpStatusCode.NoContent)
        }

        put(StudentRoutingConstants.CHANGE_ADINFO,
            {
                description = "for admin"
                queryParam<Int>(StudentRoutingConstants.studentIdQ)
                queryParam<String>(StudentRoutingConstants.adInfoQ)
            }
        ) {
            val studentId = call.getIntParam(StudentRoutingConstants.studentIdQ)
            val newAddedInfo = call.getQueryParam(StudentRoutingConstants.adInfoQ,128)
            studentController.changeAddedInfo(studentId,newAddedInfo)
            call.respond(HttpStatusCode.NoContent)
        }

    }

    authenticate(ROLES.STUDENT.name) {
        get(StudentRoutingConstants.INFO,
            basicSwaggerBuilder(
                description = "for student",
                responseBody = StudentModel::class
            )
        ){
            val studentId = call.getStudentId()
            val studentModel = studentController.findStudentModelById(studentId)
            call.respond(studentModel)
        }
    }

    authenticate {

        get(StudentRoutingConstants.GET_BY_GROUP,
            {
                description = "with token"
                queryParam<Int>(StudentRoutingConstants.groupIdQ)
            }
        ){
            val groupId = call.getIntParam(StudentRoutingConstants.groupIdQ)
            val studentModelList = studentController.getAllStudentModelByGroup(groupId)
            call.respond(studentModelList)
        }

        get(StudentRoutingConstants.GET_BY_DEPARTMENT,
            {
                description = "with token"
                queryParam<Int>(StudentRoutingConstants.departmentIdQ)
            }
        ) {
            val departmentId = call.getIntParam(StudentRoutingConstants.departmentIdQ)
            val studentModelList = studentController.getAllStudentModelByDepartment(departmentId)
            call.respond(studentModelList)
        }

        get(StudentRoutingConstants.GET_BY_ID,
            {
                description = "with token"
                queryParam<Int>(StudentRoutingConstants.studentIdQ)
                responseBody(StudentModel::class)
            }
        ){
            val studentId = call.getIntParam(StudentRoutingConstants.studentIdQ)
            val studentModel = studentController.findStudentModelById(studentId)
            call.respond(studentModel)
        }

        get(StudentRoutingConstants.ALL,
            basicSwaggerBuilder(
                description = "with token",
                responseBody = Array<StudentModel>::class
            )
        ){
            val studentModelList = studentController.getAllStudentModel()
            call.respond(studentModelList)
        }

    }




}
