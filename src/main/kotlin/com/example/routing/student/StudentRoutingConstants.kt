package com.example.routing.student



object StudentRoutingConstants {

    const val studentIdQ = "studentId"
    const val groupIdQ = "groupId"
    const val departmentIdQ = "departmentIdQ"
    const val adInfoQ = "addedInfo"

    const val INFO = "info"
    const val CREATE = "create"
    const val DELETE = "delete"
    const val ALL = "all"
    const val CHANGE_GROUP = "changeGroup"
    const val CHANGE_ADINFO = "change-adinfo"
    const val GET_BY_GROUP = "get-by-group"
    const val GET_BY_DEPARTMENT = "get-by-department"
    const val GET_BY_ID = "get-by-id"
}