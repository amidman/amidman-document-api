package com.example.routing.department




object DepartmentRoutingConstants {

    const val departmentIdQ = "departmentId"
    const val departmentNameQ = "departmentName"
    const val departmentShortNameQ = "departmentShortName"

    const val CREATE = "create"
    const val ALL = "all"
    const val CHANGE_SHORT_NAME = "change-short-name"
    const val DELETE = "delete"
    const val CHANGE_NAME = "change-name"
    const val GET_BY_NAME = "get-by-name"
    const val GET_BY_ID = "get-by-id"
}