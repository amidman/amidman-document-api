package com.example.routing.department

import com.example.domain.controller.DepartmentController
import com.example.domain.model.department.DepartmentDTO
import com.example.domain.model.department.DepartmentModel
import com.example.plugins.basicSwaggerBuilder
import com.example.plugins.queryParam
import com.example.plugins.responseBody
import com.example.routing.getIntParam
import com.example.routing.getQueryParam
import com.example.security.middleware.ROLES
import io.github.smiley4.ktorswaggerui.dsl.get
import io.github.smiley4.ktorswaggerui.dsl.post
import io.github.smiley4.ktorswaggerui.dsl.put
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject


fun Route.departmentRouting(){


    val departmentController by inject<DepartmentController>()

    authenticate(ROLES.ADMIN.name) {

        post(DepartmentRoutingConstants.CREATE,
            basicSwaggerBuilder(
                description = "for admin",
                requestBody = DepartmentDTO::class,
                responseBody = DepartmentDTO::class
            )
        ) {
            val department = call.receive<DepartmentDTO>()
            val departmentDTO = departmentController.createDepartment(department)
            call.respond(departmentDTO)
        }

        put(DepartmentRoutingConstants.CHANGE_NAME,
            {
                description = "for admin"
                queryParam<Int>(DepartmentRoutingConstants.departmentIdQ)
                queryParam<String>(DepartmentRoutingConstants.departmentNameQ)
            }
        ) {
            val departmentId = call.getIntParam(DepartmentRoutingConstants.departmentIdQ)
            val departmentName = call.getQueryParam(DepartmentRoutingConstants.departmentNameQ,128)
            departmentController.changeDepartmentName(departmentId,departmentName)
            call.respond(HttpStatusCode.NoContent)
        }

        put(DepartmentRoutingConstants.CHANGE_SHORT_NAME,
            {
                description = "for admin"
                queryParam<Int>(DepartmentRoutingConstants.departmentIdQ)
                queryParam<String>(DepartmentRoutingConstants.departmentShortNameQ)
            }
        ) { 
            val departmentId = call.getIntParam(DepartmentRoutingConstants.departmentIdQ)
            val departmentShortName = call.getQueryParam(DepartmentRoutingConstants.departmentShortNameQ,8)
            departmentController.changeDepartmentShortName(departmentId,departmentShortName)
            call.respond(HttpStatusCode.NoContent)
        }


    }


    authenticate {
        get(DepartmentRoutingConstants.ALL,
            basicSwaggerBuilder(
                description = "with token",
                responseBody = Array<DepartmentModel>::class
            )
        ){
            val departmentModelList = departmentController.getAllDepartmentModel()
            call.respond(departmentModelList)
        }

        get(DepartmentRoutingConstants.GET_BY_ID,
            {
                description = "with token"
                queryParam<Int>(DepartmentRoutingConstants.departmentIdQ)
                responseBody(DepartmentModel::class)
            }
        ) {
            val departmentId = call.getIntParam(DepartmentRoutingConstants.departmentIdQ)
            val departmentModel = departmentController.findDepartmentModelById(departmentId)
            call.respond(departmentModel)
        }

        get(DepartmentRoutingConstants.GET_BY_NAME,
            {
                description = "with token"
                queryParam<String>(DepartmentRoutingConstants.departmentNameQ)
                responseBody(DepartmentModel::class)
            }
        ) {
            val departmentName = call.getQueryParam(DepartmentRoutingConstants.departmentNameQ,128)
            val departmentModel = departmentController.findDepartmentModelByName(departmentName)
            call.respond(departmentModel)
        }
    }
}