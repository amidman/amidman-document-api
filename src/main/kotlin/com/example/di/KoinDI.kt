package com.example.di

import io.ktor.server.application.*
import org.koin.ktor.plugin.Koin

fun Application.configureKoinDI(){

    install(Koin){
        val config = this@configureKoinDI.environment.config
        entityModules()

        modules(
            configModule(config),
            fileWorkerModule
        )
    }
}