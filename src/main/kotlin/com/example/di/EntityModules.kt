package com.example.di

import com.example.di.entity_modules.*
import org.koin.core.KoinApplication

fun KoinApplication.entityModules(){
    modules(
        userModule,
        studentModule,
        groupModule,
        studentDocumentModule,
        departmentModule,
        studentRequestModule
    )
}