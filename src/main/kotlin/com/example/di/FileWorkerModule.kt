package com.example.di

import com.example.domain.fileworker.StudyDocumentFileWorker
import com.example.domain.fileworker.basic.MainFileWorker
import org.koin.dsl.module

val fileWorkerModule = module {


    single<StudyDocumentFileWorker>{
        StudyDocumentFileWorker(
            fileConfig = get(),
            fontConfig = get(),
        )
    }

    single<MainFileWorker> {
        MainFileWorker(
            studyDocumentFileWorker = get()
        )
    }
}