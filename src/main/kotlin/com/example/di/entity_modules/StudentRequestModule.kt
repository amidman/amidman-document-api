package com.example.di.entity_modules

import com.example.database.dao.student_request.StudentRequestDaoImpl
import com.example.domain.controller.StudentRequestController
import com.example.domain.dao.StudentRequestDao
import org.koin.dsl.module


val studentRequestModule = module {
    single<StudentRequestDao>{
        StudentRequestDaoImpl
    }
    single<StudentRequestController>{
        StudentRequestController(
            studentRequestDao = get(),
            studentDao = get(),
            mainFileWorker = get(),
            departmentDao = get(),
            groupDao = get(),
        )
    }
}