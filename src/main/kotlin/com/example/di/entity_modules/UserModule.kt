package com.example.di.entity_modules

import com.example.database.dao.user.UserDaoImpl
import com.example.domain.controller.UserController
import com.example.domain.dao.UserDao
import com.example.security.middleware.TokenProvider
import com.example.security.middleware.TokenProviderImpl
import org.koin.dsl.module

val userModule = module {
    single<UserDao>{
        UserDaoImpl
    }
    single<TokenProvider>{
        TokenProviderImpl(
            jwtConfig = get()
        )
    }
    single<UserController> {
        UserController(
            userDao = get(),
            tokenProvider = get(),
            studentDao = get()
        )
    }
}