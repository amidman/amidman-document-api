package com.example.di.entity_modules

import com.example.database.dao.student.StudentDaoImpl
import com.example.domain.controller.StudentController
import com.example.domain.dao.StudentDao
import org.koin.dsl.module

val studentModule = module {
    single<StudentDao>{
        StudentDaoImpl
    }

    single<StudentController>{
        StudentController(
            studentDao = get(),
            groupDao = get(),
            studentDocumentDao = get(),
            userDao = get(),
            departmentDao = get(),
        )
    }
}