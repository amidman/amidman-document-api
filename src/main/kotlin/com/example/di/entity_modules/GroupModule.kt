package com.example.di.entity_modules

import com.example.database.dao.group.GroupDaoImpl
import com.example.domain.controller.GroupController
import com.example.domain.dao.GroupDao
import org.koin.dsl.module

val groupModule = module {
    single<GroupDao>{
        GroupDaoImpl
    }
    single<GroupController>{
        GroupController(
            groupDao = get(),
            departmentDao = get(),
            studentDao = get()
        )
    }
}