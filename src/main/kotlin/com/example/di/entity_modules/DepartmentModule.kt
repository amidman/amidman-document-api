package com.example.di.entity_modules

import com.example.database.dao.department.DepartmentDaoImpl
import com.example.domain.controller.DepartmentController
import com.example.domain.dao.DepartmentDao
import org.koin.dsl.module

val departmentModule = module {
    single<DepartmentDao> {
        DepartmentDaoImpl
    }

    single<DepartmentController>{
        DepartmentController(
            departmentDao = get(),
            groupDao = get()
        )
    }
}