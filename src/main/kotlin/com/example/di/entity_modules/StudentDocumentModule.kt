package com.example.di.entity_modules

import com.example.database.dao.student_document.StudentDocumentDaoImpl
import com.example.domain.dao.StudentDocumentDao
import org.koin.dsl.module

val studentDocumentModule = module {
    single<StudentDocumentDao>{
        StudentDocumentDaoImpl
    }

}