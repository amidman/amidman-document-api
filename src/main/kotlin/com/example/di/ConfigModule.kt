package com.example.di

import com.example.domain.model.config.*
import io.ktor.server.application.*
import io.ktor.server.config.*
import org.koin.core.module.Module
import org.koin.dsl.module


fun configModule(config:ApplicationConfig):Module = module{
    val jwtAudience = config.property("jwt.audience").getString()
    val realm = config.property("jwt.realm").getString()
    val secret = config.property("jwt.secret").getString()
    val issuer = config.property("jwt.domain").getString()
    val user = config.property("database.user").getString()
    val password = config.property("database.password").getString()
    val dbUrl = config.property("database.dbUrl").getString()
    val driverName = config.property("database.driverName").getString()
    val host = config.host
    val port = config.port
    val swaggerHost = config.property("ktor.deployment.swagger-host").getString()
    val docRoot = config.property("files.root").getString()
    val nobudgetStudyDocument = config.property("files.nobudget_study_document").getString()
    val budgetStudyDocument = config.property("files.budget_study_document").getString()
    val fontRoot = config.property("fonts.root").getString()
    val timesNewRoman = config.property("fonts.times_new_roman").getString()
    single<ApplicationConfigData> {
        ApplicationConfigData(
            host = host,
            port = port,
            swagger_host = swaggerHost
        )
    }

    single<DatabaseConfig> {
        DatabaseConfig(
            user = user,
            password = password,
            dbUrl = dbUrl,
            driverName = driverName
        )
    }

    single<JWTConfig>{
        JWTConfig(
            audience = jwtAudience,
            realm = realm,
            secret = secret,
            issuer = issuer
        )
    }

    single<FileConfig>{
        FileConfig(
            root = docRoot,
            budgetStudyDocument = docRoot + budgetStudyDocument,
            nobudgetStudyDocument = docRoot + nobudgetStudyDocument
        )
    }

    single<FontConfig>{
        FontConfig(
            root = fontRoot,
            timesNewRoman = timesNewRoman
        )
    }
}