package com.example.domain.model.student_document

import com.example.errors.entity_errors.BasicErrors
import com.example.errors.utils.ResponseException
import kotlinx.datetime.LocalDate
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class StudentDocumentDTOValidationTest {

    @Test
    fun `empty fields validation test`() {
        val date = LocalDate.parse("2022-12-31")
        val emptyOrderNumberDocumentDTO = StudentDocumentDTO(
            id = null, docNumber = "no-empty", orderNumber = "", orderDate = date, studyStartDate = date
        )
        val emptyOrderNumberException = assertFailsWith<ResponseException> {
            emptyOrderNumberDocumentDTO.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS, emptyOrderNumberException)
        val emptyDocNumberDocumentDTO = emptyOrderNumberDocumentDTO.copy(
            docNumber = "",
            orderNumber = "no-empty"
        )
        val emptyDocNumberException = assertFailsWith<ResponseException> {
            emptyDocNumberDocumentDTO.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS, emptyDocNumberException)
        val rightValidationDocumentDTO = emptyOrderNumberDocumentDTO.copy(orderNumber = "no-empty")
        rightValidationDocumentDTO.validate()
    }

    @Test
    fun `many symbols in document number test`() {
        val wrongDocNumberLength = "a".repeat(StudentDocumentConstants.DOC_NUMBER_FIELD_SIZE + 1)
        assertEquals(wrongDocNumberLength.length, StudentDocumentConstants.DOC_NUMBER_FIELD_SIZE + 1)
        val date = LocalDate.parse("2022-12-31")
        val wrongDocNumberDocumentDTO = StudentDocumentDTO(
            id = null,
            docNumber = wrongDocNumberLength,
            orderNumber = "no-empty",
            orderDate = date,
            studyStartDate = date
        )
        val wrongDocNumberLengthException = assertFailsWith<ResponseException> {
            wrongDocNumberDocumentDTO.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = StudentDocumentConstants.DOC_NUMBER_FIELD,
                size = StudentDocumentConstants.DOC_NUMBER_FIELD_SIZE
            ),
            wrongDocNumberLengthException
        )
        val maxDocNumberLength = "a".repeat(StudentDocumentConstants.DOC_NUMBER_FIELD_SIZE)
        assertEquals(StudentDocumentConstants.DOC_NUMBER_FIELD_SIZE, maxDocNumberLength.length)
        val maxDocNumberLengthDocumentDTO = wrongDocNumberDocumentDTO.copy(docNumber = maxDocNumberLength)
        maxDocNumberLengthDocumentDTO.validate()
    }

    @Test
    fun `many symbols in order number test`() {
        val wrongOrderNumberLength = "a".repeat(StudentDocumentConstants.ORDER_NUMBER_FIELD_SIZE + 1)
        assertEquals(StudentDocumentConstants.ORDER_NUMBER_FIELD_SIZE + 1, wrongOrderNumberLength.length)
        val date = LocalDate.parse("2022-12-31")
        val wrongOrderNumberDocumentDTO = StudentDocumentDTO(
            id = null,
            docNumber = "no-empty",
            orderNumber = wrongOrderNumberLength,
            orderDate = date,
            studyStartDate = date
        )
        val wrongOrderNumberLengthException = assertFailsWith<ResponseException> {
            wrongOrderNumberDocumentDTO.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = StudentDocumentConstants.ORDER_NUMBER_FIELD,
                size = StudentDocumentConstants.ORDER_NUMBER_FIELD_SIZE
            ),
            wrongOrderNumberLengthException
        )
        val maxOrderNumberLength = "a".repeat(StudentDocumentConstants.ORDER_NUMBER_FIELD_SIZE)
        assertEquals(StudentDocumentConstants.ORDER_NUMBER_FIELD_SIZE, maxOrderNumberLength.length)
        val maxOrderNumberLengthDocumentDTO = wrongOrderNumberDocumentDTO.copy(orderNumber = maxOrderNumberLength)
        maxOrderNumberLengthDocumentDTO.validate()
    }
}