package com.example.domain.model.student

import com.example.errors.entity_errors.BasicErrors
import com.example.errors.utils.ResponseException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.test.assertFailsWith

class StudentDTOValidationTest {

    @Test
    fun `empty added info test`() {
        val emptyAddedInfoStudentDTO = StudentDTO(
            id = null,
            userId = 5393,
            groupId = 8372,
            studentDocumentId = 6169,
            addedInfo = ""
        )
        val emptyAddedInfoException = assertFailsWith<ResponseException> {
            emptyAddedInfoStudentDTO.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS, emptyAddedInfoException)
        val noEmptyStudentDTO = emptyAddedInfoStudentDTO.copy(addedInfo = "no-empty")
        noEmptyStudentDTO.validate()
    }

    @Test
    fun `many symbols in added info test`() {
        val addedInfoWrongLength = "a".repeat(StudentConstants.ADDED_INFO_FIELD_SIZE + 1)
        assertEquals(StudentConstants.ADDED_INFO_FIELD_SIZE + 1, addedInfoWrongLength.length)
        val addedInfoManySymbolsStudentDTO = StudentDTO(
            id = null,
            userId = 221,
            groupId = 6953,
            studentDocumentId = 3711,
            addedInfo = addedInfoWrongLength
        )
        val addedInfoManySymbolsException = assertFailsWith<ResponseException> {
            addedInfoManySymbolsStudentDTO.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = StudentConstants.ADDED_INFO_FIELD,
                size = StudentConstants.ADDED_INFO_FIELD_SIZE
            ),
            addedInfoManySymbolsException
        )
        val maxAddedInfoLength = "a".repeat(StudentConstants.ADDED_INFO_FIELD_SIZE)
        assertEquals(StudentConstants.ADDED_INFO_FIELD_SIZE,maxAddedInfoLength.length,)
        val maxAddedInfoStudentDTO = addedInfoManySymbolsStudentDTO.copy(addedInfo = maxAddedInfoLength)
        maxAddedInfoStudentDTO.validate()
    }
}