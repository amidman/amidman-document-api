package com.example.domain.model.department

import com.example.errors.entity_errors.BasicErrors
import com.example.errors.utils.ResponseException
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith


class DepartmentDTOValidationTest {

    @Test
    fun `empty values validation test`() {
        val departmentWithEmptyName = DepartmentDTO(
            id = null,
            departmentName = "",
            departmentShortName = "no-empty"
        )
        val departmentEmptyNameException = assertFailsWith<ResponseException> {
            departmentWithEmptyName.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS,departmentEmptyNameException)
        val departmentWithEmptyShortName = DepartmentDTO(
            id = null,
            departmentName = "no-empty",
            departmentShortName = ""
        )
        val departmentWithEmptyShortNameException = assertFailsWith<ResponseException> {
            departmentWithEmptyShortName.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS,departmentWithEmptyShortNameException)
        val departmentWithEmptyFields = DepartmentDTO(
            id = null,
            departmentShortName = " ",
            departmentName = " "
        )
        val departmentWithEmptyFieldsException = assertFailsWith<ResponseException> {
            departmentWithEmptyFields.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS,departmentWithEmptyFieldsException)
        val departmentDTOValidateClear = DepartmentDTO(
            id = null,
            departmentName = "a",
            departmentShortName = "a"
        )
        departmentDTOValidateClear.validate()
    }

    @Test
    fun `short name many symbols validation test`() {
        val wrongShortNameLength = "a".repeat(DepartmentConstants.SHORT_NAME_FIELD_SIZE + 1)
        assertEquals(DepartmentConstants.SHORT_NAME_FIELD_SIZE + 1,wrongShortNameLength.length, )
        val departmentDTOWithValidateError = DepartmentDTO(
            id = null,
            departmentName = "a",
            departmentShortName = wrongShortNameLength
        )
        val departmentShortNameException = assertFailsWith<ResponseException> {
            departmentDTOWithValidateError.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                size = DepartmentConstants.SHORT_NAME_FIELD_SIZE,
                fieldName = DepartmentConstants.SHORT_NAME_FIELD
            ),
            departmentShortNameException
        )
        val maxClearShortNameLength = "a".repeat(DepartmentConstants.SHORT_NAME_FIELD_SIZE)
        assertEquals(DepartmentConstants.SHORT_NAME_FIELD_SIZE,maxClearShortNameLength.length)
        val departmentDTOValidateClear = DepartmentDTO(
            id = null,
            departmentShortName = "bbbbbbbb",
            departmentName = "a"
        )
        departmentDTOValidateClear.validate()
    }

    @Test
    fun `name many symbols validation test`() {
        val wrongNameLength = "a".repeat(DepartmentConstants.NAME_FIELD_SIZE + 1)
        assertEquals(DepartmentConstants.NAME_FIELD_SIZE + 1,wrongNameLength.length, )
        val departmentDTOWithValidateError = DepartmentDTO(
            id = null,
            departmentName = wrongNameLength,
            departmentShortName = "a"
        )
        val exception = assertFailsWith<ResponseException> {
            departmentDTOWithValidateError.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = DepartmentConstants.NAME_FIELD,
                size = DepartmentConstants.NAME_FIELD_SIZE
            ),
            exception
        )
        val maxClearNameLength = "a".repeat(DepartmentConstants.NAME_FIELD_SIZE)
        assertEquals(DepartmentConstants.NAME_FIELD_SIZE,maxClearNameLength.length,)
        val departmentDTOValidateClear = DepartmentDTO(
            id = null,
            departmentShortName = "a",
            departmentName = maxClearNameLength
        )
        departmentDTOValidateClear.validate()
    }
}