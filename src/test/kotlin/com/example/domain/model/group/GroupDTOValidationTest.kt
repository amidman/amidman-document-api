package com.example.domain.model.group

import com.example.errors.entity_errors.BasicErrors
import com.example.errors.entity_errors.GroupErrors
import com.example.errors.utils.ResponseException
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith


class GroupDTOValidationTest {

    @Test
    fun `empty name field test`() {
        val emptyNameGroupDTO = GroupDTO(
            groupName = "   ", status = GroupStatus.BUDGET, currentYearCount = 1, maxYearCount = 4, departmentId = 1
        )
        val emptyGroupNameException = assertFailsWith<ResponseException> {
            emptyNameGroupDTO.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS, emptyGroupNameException)
        val noEmptyNameGroupDTO = emptyNameGroupDTO.copy(groupName = "no-empty")
        noEmptyNameGroupDTO.validate()
    }

    @Test
    fun `many symbols group name test`() {
        val wrongNameLength = "a".repeat(GroupConstants.NAME_FIELD_SIZE + 1)
        assertEquals(GroupConstants.NAME_FIELD_SIZE + 1, wrongNameLength.length)
        val wrongNameLengthGroupDTO = GroupDTO(
            groupName = wrongNameLength,
            status = GroupStatus.BUDGET,
            currentYearCount = 1,
            maxYearCount = 4,
            departmentId = 1
        )
        val wrongNameLengthException = assertFailsWith<ResponseException> {
            wrongNameLengthGroupDTO.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = GroupConstants.NAME_FIELD,
                size = GroupConstants.NAME_FIELD_SIZE
            ),
            wrongNameLengthException
        )
        val maxNameLength = "a".repeat(GroupConstants.NAME_FIELD_SIZE)
        assertEquals(GroupConstants.NAME_FIELD_SIZE, maxNameLength.length)
        val maxNameLengthGroupDTO = wrongNameLengthGroupDTO.copy(groupName = maxNameLength)
        maxNameLengthGroupDTO.validate()
    }

    @Test
    fun `currentYearCount in yearCountRange test`() {
        val wrongYearCount = GroupConstants.yearCountRange.last + 1
        val maxYearCount = GroupConstants.maxYearCountRange.last
        val wrongYearCountGroupDTO = GroupDTO(
            id = null,
            groupName = "a",
            status = GroupStatus.BUDGET,
            currentYearCount = wrongYearCount,
            maxYearCount = maxYearCount,
            departmentId = 1
        )
        val wrongYearCountException = assertFailsWith<ResponseException> {
            wrongYearCountGroupDTO.validate()
        }
        assertEquals(GroupErrors.WRONG_GROUP_YEAR_COUNT, wrongYearCountException)
        for (yearCount in GroupConstants.yearCountRange) {
            val groupDTO = wrongYearCountGroupDTO.copy(currentYearCount = yearCount)
            groupDTO.validate()
        }
    }

    @Test
    fun `maxYearCount in maxYearCount range test`() {
        val currentYearCount = GroupConstants.yearCountRange.first
        val wrongMaxYearCount = GroupConstants.maxYearCountRange.last + 1
        val wrongMaxYearCountGroupDTO = GroupDTO(
            groupName = "a",
            status = GroupStatus.BUDGET,
            currentYearCount = currentYearCount,
            maxYearCount = wrongMaxYearCount,
            departmentId = 1
        )
        val wrongMaxYearCountException = assertFailsWith<ResponseException> {
            wrongMaxYearCountGroupDTO.validate()
        }
        assertEquals(GroupErrors.WRONG_GROUP_MAX_YEAR_COUNT, wrongMaxYearCountException)
        for (maxYearCount in GroupConstants.maxYearCountRange) {
            val groupDTO = wrongMaxYearCountGroupDTO.copy(maxYearCount = maxYearCount)
            groupDTO.validate()
        }
    }

    @Test
    fun `current year count is not bigger than max year count test`() {
        val currentYearCount = GroupConstants.yearCountRange.last
        val maxYearCount = GroupConstants.maxYearCountRange.first
        val wrongGroupDTO = GroupDTO(
            groupName = "a",
            status = GroupStatus.NOBUGET,
            currentYearCount = currentYearCount,
            maxYearCount = maxYearCount,
            departmentId = 1
        )
        val wrongGroupDTOException = assertFailsWith<ResponseException> {
            wrongGroupDTO.validate()
        }
        assertEquals(GroupErrors.WRONG_GROUP_YEAR_COUNT, wrongGroupDTOException)
        val rightGroupDTO = wrongGroupDTO.copy(maxYearCount = wrongGroupDTO.currentYearCount)
        rightGroupDTO.validate()
        val secondCaseRightGroupDTO = wrongGroupDTO.copy(currentYearCount = wrongGroupDTO.maxYearCount)
        secondCaseRightGroupDTO.validate()
    }

}