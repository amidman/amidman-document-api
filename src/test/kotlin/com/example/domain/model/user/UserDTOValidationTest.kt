package com.example.domain.model.user

import com.example.errors.entity_errors.BasicErrors
import com.example.errors.utils.ResponseException
import com.example.security.middleware.ROLES
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.test.assertFailsWith

class UserDTOValidationTest {

    @Test
    fun `empty fields test`() {
        UserDTO(
            id = null,
            login = "Diondra",
            password = "Josephina",
            name = "Mesha",
            surname = "Lyndsi",
            fatherName = "Marice",
            role = ROLES.ADMIN
        )
        val emptyLoginUserDTO = UserDTO(
            id = null,
            login = "",
            password = "Josephina",
            name = "Mesha",
            surname = "Lyndsi",
            fatherName = "Marice",
            role = ROLES.ADMIN
        )
        val emptyLoginException = assertFailsWith<ResponseException> {
            emptyLoginUserDTO.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS, emptyLoginException)
        val emptyPasswordUserDTO = UserDTO(
            id = null,
            login = "no-empty",
            password = "",
            name = "Mesha",
            surname = "Lyndsi",
            fatherName = "Marice",
            role = ROLES.ADMIN
        )
        val emptyPasswordException = assertFailsWith<ResponseException> {
            emptyPasswordUserDTO.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS, emptyPasswordException)
        val emptyNameUserDTO = UserDTO(
            id = null,
            login = "no-empty",
            password = "no-empty",
            name = "",
            surname = "Lyndsi",
            fatherName = "Marice",
            role = ROLES.ADMIN
        )
        val emptyNameException = assertFailsWith<ResponseException> {
            emptyNameUserDTO.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS, emptyNameException)
        val emptySurnameUserDTO = UserDTO(
            id = null,
            login = "no-empty",
            password = "no-empty",
            name = "no-empty",
            surname = "",
            fatherName = "Marice",
            role = ROLES.ADMIN
        )
        val emptySurnameException = assertFailsWith<ResponseException> {
            emptySurnameUserDTO.validate()
        }
        assertEquals(BasicErrors.EMPTY_FIELDS, emptySurnameException)

        val rightValidationUserDTO = UserDTO(
            id = null,
            login = "no-empty",
            password = "no-empty",
            name = "no-empty",
            surname = "no-empty",
            fatherName = "",
            role = ROLES.ADMIN
        )
        rightValidationUserDTO.validate()
    }

    @Test
    fun `many symbol login test`() {
        val wrongLoginLength = "a".repeat(UserConstants.LOGIN_FIELD_SIZE + 1)
        assertEquals(UserConstants.LOGIN_FIELD_SIZE + 1, wrongLoginLength.length)
        val wrongLoginUserDTO = UserDTO(
            id = null,
            login = wrongLoginLength,
            password = "Jacoby",
            name = "Giorgio",
            surname = "Shauntel",
            fatherName = "Garry",
            role = ROLES.ADMIN
        )
        val wrongLoginLengthException = assertFailsWith<ResponseException> {
            wrongLoginUserDTO.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = UserConstants.LOGIN_FIELD,
                size = UserConstants.LOGIN_FIELD_SIZE
            ),
            wrongLoginLengthException
        )
        val maxLoginLength = "a".repeat(UserConstants.LOGIN_FIELD_SIZE)
        assertEquals(UserConstants.LOGIN_FIELD_SIZE, maxLoginLength.length)
        val rightValidationUserDTO = wrongLoginUserDTO.copy(login = maxLoginLength)
        rightValidationUserDTO.validate()
    }

    @Test
    fun `many symbol password test`() {
        val wrongPasswordLength = "a".repeat(UserConstants.PASSWORD_FIELD_SIZE + 1)
        assertEquals(UserConstants.PASSWORD_FIELD_SIZE + 1, wrongPasswordLength.length)
        val wrongPasswordUserDTO = UserDTO(
            id = null,
            login = "Miriam",
            password = wrongPasswordLength,
            name = "Heaven",
            surname = "Anaalicia",
            fatherName = "Tung",
            role = ROLES.ADMIN
        )
        val wrongPasswordLengthException = assertFailsWith<ResponseException> {
            wrongPasswordUserDTO.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = UserConstants.PASSWORD_FIELD,
                size = UserConstants.PASSWORD_FIELD_SIZE
            ),
            wrongPasswordLengthException
        )
        val maxPasswordLength = "a".repeat(UserConstants.PASSWORD_FIELD_SIZE)
        assertEquals(UserConstants.PASSWORD_FIELD_SIZE, maxPasswordLength.length)
        val rightValidationUserDTO = wrongPasswordUserDTO.copy(password = maxPasswordLength)
        rightValidationUserDTO.validate()
    }

    @Test
    fun `many symbol name test`() {
        val wrongNameLength = "a".repeat(UserConstants.NAME_FIELD_SIZE + 1)
        assertEquals(UserConstants.NAME_FIELD_SIZE + 1, wrongNameLength.length)
        val wrongNameUserDTO = UserDTO(
            id = null,
            login = "Roxanna",
            password = "Rayford",
            name = wrongNameLength,
            surname = "Allisyn",
            fatherName = "Rebeccah",
            role = ROLES.ADMIN
        )
        val wrongNameLengthException = assertFailsWith<ResponseException> {
            wrongNameUserDTO.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = UserConstants.NAME_FIELD,
                size = UserConstants.NAME_FIELD_SIZE
            ),
            wrongNameLengthException
        )
        val maxNameLength = "a".repeat(UserConstants.NAME_FIELD_SIZE)
        assertEquals(UserConstants.NAME_FIELD_SIZE, maxNameLength.length)
        val rightValidationUserDTO = wrongNameUserDTO.copy(name = maxNameLength)
        rightValidationUserDTO.validate()
    }

    @Test
    fun `many symbol surname test`() {
        val wrongSurnameLength = "a".repeat(UserConstants.SURNAME_FIELD_SIZE + 1)
        assertEquals(UserConstants.SURNAME_FIELD_SIZE + 1, wrongSurnameLength.length)
        val wrongSurnameUserDTO = UserDTO(
            id = null,
            login = "Dori",
            password = "Zakia",
            name = "Mickel",
            surname = wrongSurnameLength,
            fatherName = "Tyrese",
            role = ROLES.ADMIN
        )
        val wrongSurnameLengthException = assertFailsWith<ResponseException> {
            wrongSurnameUserDTO.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = UserConstants.SURNAME_FIELD,
                size = UserConstants.SURNAME_FIELD_SIZE
            ),
            wrongSurnameLengthException,
        )
        val maxSurnameLength = "a".repeat(UserConstants.SURNAME_FIELD_SIZE)
        assertEquals(UserConstants.SURNAME_FIELD_SIZE, maxSurnameLength.length)
        val rightValidationUserDTO = UserDTO(
            id = null,
            login = "Tempest",
            password = "Sahar",
            name = "Ambur",
            surname = maxSurnameLength,
            fatherName = "Benita",
            role = ROLES.ADMIN
        )
        rightValidationUserDTO.validate()
    }


    @Test
    fun `many symbol father name test`() {
        val wrongFatherNameLength = "a".repeat(UserConstants.FATHER_NAME_FIELD_SIZE + 1)
        assertEquals(UserConstants.FATHER_NAME_FIELD_SIZE + 1, wrongFatherNameLength.length)
        val wrongFatherNameUserDTO = UserDTO(
            id = null,
            login = "Liesel",
            password = "Shamus",
            name = "Mason",
            surname = "Ashanti",
            fatherName = wrongFatherNameLength,
            role = ROLES.ADMIN
        )
        val wrongFatherNameLengthException = assertFailsWith<ResponseException> {
            wrongFatherNameUserDTO.validate()
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = UserConstants.FATHER_NAME_FIELD,
                size = UserConstants.FATHER_NAME_FIELD_SIZE
            ),
            wrongFatherNameLengthException
        )
        val maxFatherNameLength = "a".repeat(UserConstants.FATHER_NAME_FIELD_SIZE)
        assertEquals(UserConstants.FATHER_NAME_FIELD_SIZE, maxFatherNameLength.length)
        val rightValidationUserDTO = wrongFatherNameUserDTO.copy(fatherName = maxFatherNameLength)
        rightValidationUserDTO.validate()
    }

}