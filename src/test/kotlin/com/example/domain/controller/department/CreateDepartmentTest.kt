package com.example.domain.controller.department

import com.example.database.dao.department.DepartmentData
import com.example.domain.controller.DepartmentController
import com.example.domain.dao.DepartmentDao
import com.example.domain.dao.GroupDao
import com.example.errors.entity_errors.DepartmentErrors
import com.example.errors.utils.ResponseException
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class CreateDepartmentTest {


    @get:Before
    val departmentDao: DepartmentDao = mock()

    @get:Before
    val groupDao: GroupDao = mock()

    @get:Before
    val departmentController: DepartmentController = DepartmentController(departmentDao, groupDao)

    @Test
    fun `create department should fail test with wrong name exception`(): Unit = runBlocking {
        whenever(departmentDao.isDepartmentNameUnique(any())).thenReturn(false)
        val wrongDepartmentNameException = assertFailsWith<ResponseException> {
            departmentController.createDepartment(DepartmentData.EXIST_DEPARTMENT)
        }
        assertEquals(DepartmentErrors.WRONG_NAME,wrongDepartmentNameException, )
    }

    @Test
    fun `create success department test`(): Unit = runBlocking {
        val newID = Random.nextInt()
        whenever(departmentDao.isDepartmentNameUnique(any())).thenReturn(true)
        whenever(
            departmentDao.createAsync(DepartmentData.EXIST_DEPARTMENT)
        ).thenReturn(async { newID })
        val departmentDTO = departmentController.createDepartment(
            DepartmentData.EXIST_DEPARTMENT
        )
        assertEquals(DepartmentData.EXIST_DEPARTMENT.copy(id = newID),departmentDTO,)
    }

}