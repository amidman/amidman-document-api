package com.example.domain.controller.department

import com.example.database.dao.department.DepartmentData
import com.example.domain.controller.DepartmentController
import com.example.domain.dao.DepartmentDao
import com.example.domain.dao.GroupDao
import com.example.errors.entity_errors.DepartmentErrors
import com.example.errors.utils.ResponseException
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class DeleteDepartmentTest {


    @get:Before
    val departmentDao: DepartmentDao = mock()

    @get:Before
    val groupDao: GroupDao = mock()

    @get:Before
    val departmentController: DepartmentController = DepartmentController(departmentDao, groupDao)


    @Test
    fun `delete department dto should fails with not found exception`(): Unit = runBlocking {
        whenever(departmentDao.findByIdAsync(any())).thenReturn(async { null })
        val notFoundException = assertFailsWith<ResponseException> {
            departmentController.delete(DepartmentData.NO_EXIST_DEPARTMENT_ID)
        }
        assertEquals(DepartmentErrors.NOT_FOUND,notFoundException)
    }

    @Test
    fun `delete department dto success test`(): Unit = runBlocking {
        whenever(departmentDao.findByIdAsync(any())).thenReturn(async { DepartmentData.EXIST_DEPARTMENT })
        whenever(departmentDao.deleteAsync(any())).thenReturn(async { DepartmentData.EXIST_DEPARTMENT_ID })
        departmentController.delete(DepartmentData.EXIST_DEPARTMENT_ID)
    }
}