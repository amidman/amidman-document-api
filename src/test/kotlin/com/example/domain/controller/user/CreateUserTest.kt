package com.example.domain.controller.user

import com.example.database.dao.user.UserData
import com.example.domain.controller.UserController
import com.example.domain.dao.StudentDao
import com.example.domain.dao.UserDao
import com.example.errors.entity_errors.UserErrors
import com.example.errors.utils.ResponseException
import com.example.security.middleware.TokenProvider
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class CreateUserTest {


    @get:Before
    val userDao: UserDao = mock()

    @get:Before
    val studentDao: StudentDao = mock()

    @get:Before
    val tokenProvider: TokenProvider = mock()

    @get:Before
    val userController = UserController(
        userDao = userDao,
        studentDao = studentDao,
        tokenProvider = tokenProvider,
    )

    @Test
    fun `test should be fail with login is not unique exception`():Unit = runBlocking {
        whenever(userDao.isLoginUnique(any())).thenReturn(false)
        val wrongLoginException = assertFailsWith<ResponseException> {
            userController.registerUser(UserData.EXIST_USER)
        }
        assertEquals(UserErrors.WRONG_LOGIN,wrongLoginException)
    }


}