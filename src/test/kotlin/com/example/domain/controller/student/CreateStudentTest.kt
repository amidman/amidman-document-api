package com.example.domain.controller.student

import com.example.database.dao.group.GroupData
import com.example.database.dao.student.StudentData
import com.example.database.dao.student_document.StudentDocumentData
import com.example.database.dao.user.UserData
import com.example.domain.controller.StudentController
import com.example.domain.dao.*
import com.example.errors.entity_errors.GroupErrors
import com.example.errors.entity_errors.StudentDocumentErrors
import com.example.errors.utils.ResponseException
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class CreateStudentTest {


    @get:Before
    val studentDao: StudentDao = mock()

    @get:Before
    val groupDao: GroupDao = mock()

    @get:Before
    val studentDocumentDao: StudentDocumentDao = mock()

    @get:Before
    val userDao: UserDao = mock()

    @get:Before
    val departmentDao: DepartmentDao = mock()

    @get:Before
    val studentController = StudentController(
        studentDao = studentDao,
        groupDao = groupDao,
        studentDocumentDao = studentDocumentDao,
        userDao = userDao,
        departmentDao = departmentDao,
    )

    @Test
    fun `create student should fail with wrong doc number exception`(): Unit = runBlocking {
        whenever(studentDocumentDao.isDocNumberUnique(any())).thenReturn(false)
        whenever(groupDao.findByGroupNameAsync(any())).thenReturn(async { GroupData.EXIST_GROUP })
        val wrongDocNumberException = assertFailsWith<ResponseException> {
            studentController.createStudent(StudentData.EXIST_STUDENT_CREATE_MODEL)
        }
        assertEquals(StudentDocumentErrors.WRONG_DOC_NUMBER, wrongDocNumberException)
    }

    @Test
    fun `create student should fail with group not found exception`(): Unit = runBlocking {
        whenever(studentDocumentDao.isDocNumberUnique(any())).thenReturn(true)
        whenever(groupDao.findByGroupNameAsync(any())).thenReturn(async { null })
        val groupNotFoundException = assertFailsWith<ResponseException> {
            studentController.createStudent(StudentData.EXIST_STUDENT_CREATE_MODEL)
        }
        assertEquals(GroupErrors.NOT_FOUND, groupNotFoundException)
    }

    @Test
    fun `create student success test`(): Unit = runBlocking {
        whenever(studentDocumentDao.isDocNumberUnique(any())).thenReturn(true)
        whenever(groupDao.findByGroupNameAsync(any())).thenReturn(async { GroupData.EXIST_GROUP })
        whenever(studentDocumentDao.findByIdAsync(any())).thenReturn(async { StudentDocumentData.EXIST_STUDENT_DOCUMENT })
        whenever(studentDocumentDao.createAsync(any())).thenReturn(async { StudentDocumentData.EXIST_STUDENT_DOCUMENT_ID })
        whenever(userDao.createAsync(any())).thenReturn(async { UserData.EXIST_USER_ID })
        whenever(studentDao.createAsync(any())).thenReturn(async { StudentData.EXIST_STUDENT_ID })
        whenever(studentDao.findStudentModelAsync(any())).thenReturn(async { StudentData.EXIST_STUDENT_MODEL })

        val studentModel = studentController.createStudent(StudentData.EXIST_STUDENT_CREATE_MODEL)
        assertEquals(StudentData.EXIST_STUDENT_MODEL,studentModel)
    }

}