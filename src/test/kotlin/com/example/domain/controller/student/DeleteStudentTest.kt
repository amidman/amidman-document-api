package com.example.domain.controller.student

import com.example.database.dao.student.StudentData
import com.example.database.dao.student_document.StudentDocumentData
import com.example.database.dao.user.UserData
import com.example.domain.controller.StudentController
import com.example.domain.dao.StudentDao
import com.example.domain.dao.StudentDocumentDao
import com.example.domain.dao.UserDao
import com.example.errors.entity_errors.StudentErrors
import com.example.errors.utils.ResponseException
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class DeleteStudentTest {

    @get:Before
    val studentDao: StudentDao = mock()

    @get:Before
    val userDao: UserDao = mock()

    @get:Before
    val studentDocumentDao: StudentDocumentDao = mock()

    @get:Before
    val studentController = StudentController(
        groupDao = mock(),
        departmentDao = mock(),
        studentDao = studentDao,
        studentDocumentDao = studentDocumentDao,
        userDao = userDao,
    )

    @Test
    fun `test should fail with student not found exception`(): Unit = runBlocking {
        whenever(studentDao.findByIdAsync(any())).thenReturn(async { null })
        whenever(studentDocumentDao.deleteAsync(any())).thenReturn(async { StudentDocumentData.EXIST_STUDENT_DOCUMENT_ID })
        whenever(userDao.deleteAsync(any())).thenReturn(async { UserData.EXIST_USER_ID })

        val studentNotFoundException = assertFailsWith<ResponseException> {
            studentController.delete(StudentData.EXIST_STUDENT_ID)
        }
        assertEquals(StudentErrors.NOT_FOUND,studentNotFoundException)
    }

    @Test
    fun `delete student success test`():Unit = runBlocking {
        whenever(studentDao.findByIdAsync(any())).thenReturn(async { StudentData.EXIST_STUDENT })
        whenever(studentDocumentDao.deleteAsync(any())).thenReturn(async { StudentDocumentData.EXIST_STUDENT_DOCUMENT_ID })
        whenever(userDao.deleteAsync(any())).thenReturn(async { UserData.EXIST_USER_ID })

        studentController.delete(StudentData.EXIST_STUDENT_ID)
    }
}