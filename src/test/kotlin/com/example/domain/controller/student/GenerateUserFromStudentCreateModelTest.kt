package com.example.domain.controller.student

import com.example.domain.controller.StudentController
import com.example.domain.model.student.StudentCreateModel
import com.example.domain.model.student_document.StudentDocumentDTO
import com.example.domain.model.user.UserFIO
import com.example.domain.utils.PasswordHashing
import kotlinx.datetime.LocalDate
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class GenerateUserFromStudentCreateModelTest {


    @Test
    fun `generate student login from fio test`() {
        val studentCreateModel = StudentCreateModel(
            fio = UserFIO(
                name = "Иван",
                surname = "Иванов",
                fatherName = "",
            ),
            document = StudentDocumentDTO(
                id = null,
                docNumber = "Dino",
                orderNumber = "Jasmaine",
                orderDate = LocalDate.parse("2022-01-01"),
                studyStartDate = LocalDate.parse("2022-01-01")
            ),
            addedInfo = "Davita",
            groupName = "Dain",
        )

        val user = StudentController.generateUserFromStudentCreateModel(studentCreateModel)
        assertEquals("ИвановИ", user.login)
        PasswordHashing.checkPassword(user.password,"ИвановИ")
    }
}