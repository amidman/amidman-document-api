package com.example.domain.controller.group

import com.example.database.dao.department.DepartmentData
import com.example.database.dao.group.GroupData
import com.example.domain.controller.GroupController
import com.example.domain.dao.DepartmentDao
import com.example.domain.dao.GroupDao
import com.example.domain.dao.StudentDao
import com.example.errors.entity_errors.DepartmentErrors
import com.example.errors.entity_errors.GroupErrors
import com.example.errors.utils.ResponseException
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class CreateGroupTest {


    @get:Before
    val groupDao: GroupDao = mock()

    @get:Before
    val departmentDao: DepartmentDao = mock()

    @get:Before
    val studentDao: StudentDao = mock()

    @get:Before
    val groupController = GroupController(
            groupDao = groupDao,
            departmentDao = departmentDao,
            studentDao = studentDao,
        )

    @Test
    fun `should ends with wrong name exception`():Unit = runBlocking {
        val newID = Random.nextInt()
        whenever(groupDao.isGroupNameUnique(any())).thenReturn(false)
        whenever(departmentDao.findByIdAsync(any())).thenReturn(async { DepartmentData.EXIST_DEPARTMENT })
        whenever(groupDao.createAsync(any())).thenReturn(async { newID })
        val wrongNameException = assertFailsWith<ResponseException> {
            groupController.createGroup(GroupData.EXIST_GROUP)
        }
        assertEquals(wrongNameException, GroupErrors.WRONG_NAME)
    }

    @Test
    fun `should end with department not found exception`():Unit = runBlocking{
        val newID = Random.nextInt()
        whenever(groupDao.isGroupNameUnique(any())).thenReturn(true)
        whenever(departmentDao.findByIdAsync(any())).thenReturn(async { null })
        whenever(groupDao.createAsync(any())).thenReturn(async { newID })
        val departmentNotFoundException = assertFailsWith<ResponseException> {
            groupController.createGroup(GroupData.EXIST_GROUP)
        }
        assertEquals(DepartmentErrors.NOT_FOUND,departmentNotFoundException)
    }

    @Test
    fun `success create group test`():Unit = runBlocking {
        val newID = Random.nextInt()
        whenever(groupDao.isGroupNameUnique(any())).thenReturn(true)
        whenever(departmentDao.findByIdAsync(any())).thenReturn(async { DepartmentData.EXIST_DEPARTMENT })
        whenever(groupDao.createAsync(any())).thenReturn(async { newID })
        val groupDTO = groupController.createGroup(GroupData.EXIST_GROUP)
        assertEquals(newID,groupDTO.id)
        assertEquals(DepartmentData.EXIST_DEPARTMENT_ID,groupDTO.departmentId)
    }

}