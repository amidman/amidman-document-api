package com.example.domain.utils

import com.example.errors.entity_errors.UserErrors
import com.example.errors.utils.ResponseException
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFailsWith


class EmailCheckKtTest {

    @Test
    fun `test on wrong email, should throw WRONG_EMAIL exception`(){
        val wrongEmail = "alibaba@.ru"
        val wrongEmailException = assertFailsWith<ResponseException> {
            wrongEmail.checkEmail()
        }
        assertEquals(UserErrors.WRONG_EMAIL,wrongEmailException)
        val wrongEmail2 = "alibaba@"
        val wrongEmailException2 = assertFailsWith<ResponseException> {
            wrongEmail2.checkEmail()
        }
        assertEquals(UserErrors.WRONG_EMAIL,wrongEmailException2)
        val wrongEmail3 = "alibaba@mail.r"
        val wrongEmailException3= assertFailsWith<ResponseException> {
            wrongEmail3.checkEmail()
        }
        assertEquals(UserErrors.WRONG_EMAIL,wrongEmailException3)
    }

    @Test
    fun `check emails which should pass email validation test`(){
        val email = "alibaba@mail.ru"
        email.checkEmail()
        val email2 = "gogatup96@gmail.com"
        email2.checkEmail()
    }


}