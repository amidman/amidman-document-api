package com.example.domain.utils

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

class PasswordHashingKtTest {

    @Test
    fun `hash test`(){
        val password = "my-password"
        val hashPassword = PasswordHashing.hashPassword(password)
        assertNotEquals(password,hashPassword)
    }

    @Test
    fun `check password test`(){
        val password = "my-password"
        val hashPassword = PasswordHashing.hashPassword(password)
        PasswordHashing.checkPassword(hashPassword,password)
    }
}