package com.example.domain.utils

import kotlinx.datetime.LocalDate
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class DateTimeUtilsKtTest {

    @Test
    fun `toHumanFormat function test`() {
        val localDate = LocalDate.parse("1970-01-01")
        val humanFormatString = localDate.toHumanFormat()
        val expectedHumanFormatString = "01.01.1970"
        assertEquals(expectedHumanFormatString,humanFormatString)
    }
}