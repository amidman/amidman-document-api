package com.example.domain.utils

import com.example.errors.entity_errors.BasicErrors
import com.example.errors.utils.ResponseException
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class StringValidateKtTest {

    @Test
    fun `basic string validation test`() {
        val testString = "hello-world"
        val validateTestStringMaxLength = testString.length - 1
        val validateTestStringFieldName = "test string"
        val emptyString = ""
        val manyWordsException = assertFailsWith<ResponseException> {
            testString.validate(maxLength = validateTestStringMaxLength, fieldName = validateTestStringFieldName)
        }
        assertEquals(
            BasicErrors.MANY_SYMBOLS(
                fieldName = validateTestStringFieldName,
                size = validateTestStringMaxLength
            ),
            manyWordsException
        )
        val emptyFieldException = assertFailsWith<ResponseException> {
            emptyString.validate(1)
        }
        assertEquals(BasicErrors.EMPTY_FIELDS, emptyFieldException)
        val rightValidateTestStringLength = testString.length
        testString.validate(maxLength = rightValidateTestStringLength, fieldName = validateTestStringFieldName)
    }
}