package com.example.database.dao.student_document

import com.example.domain.model.student_document.StudentDocumentDTO
import kotlinx.datetime.LocalDate

object StudentDocumentData {

    const val EXIST_STUDENT_DOCUMENT_ID = 1
    val EXIST_STUDENT_DOCUMENT = StudentDocumentDTO(
        id = EXIST_STUDENT_DOCUMENT_ID,
        docNumber = "123456",
        orderNumber = "654321",
        orderDate = LocalDate.parse("2020-08-26"),
        studyStartDate = LocalDate.parse("2020-09-01")
    )
}