package com.example.database.dao.group

import com.example.database.dao.department.DepartmentData
import com.example.domain.model.group.GroupDTO
import com.example.domain.model.group.GroupStatus

object GroupData {
    const val EXIST_GROUP_ID = 1
    const val NO_EXIST_GROUP_ID = 2000
    val EXIST_GROUP = GroupDTO(
        id = EXIST_GROUP_ID,
        groupName = "ИСП-335",
        status = GroupStatus.BUDGET,
        currentYearCount = 3,
        maxYearCount = 4,
        departmentId = DepartmentData.EXIST_DEPARTMENT_ID
    )
    val NO_EXIST_GROUP = GroupDTO(
        id = NO_EXIST_GROUP_ID,
        groupName = "исп-1111",
        status = GroupStatus.NOBUGET,
        currentYearCount = 3,
        maxYearCount = 4,
        departmentId = DepartmentData.NO_EXIST_DEPARTMENT_ID
    )

    val groupList = mutableListOf<GroupDTO>(EXIST_GROUP)
}