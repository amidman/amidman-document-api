package com.example.database.dao.user

import com.example.domain.model.user.UserDTO
import com.example.domain.model.user.UserFIO
import com.example.domain.utils.PasswordHashing
import com.example.security.middleware.ROLES

object UserData {


    val REAL_USER_PASSWORD = "test-password"
    val BCRYPT_USER_PASSWORD = PasswordHashing.hashPassword(REAL_USER_PASSWORD)
    val EXIST_USER_ID = 1

    val EXIST_USER_FIO = UserFIO(name = "Xochitl", surname = "Rodrigo", fatherName = "Britnie")

    val EXIST_USER = UserDTO(
        id = EXIST_USER_ID,
        name = "Xochitl", surname = "Rodrigo", fatherName = "Britnie",
        login = "Pofig",
        password = BCRYPT_USER_PASSWORD,
        role = ROLES.STUDENT,
        email = "example@mail.ru"
    )
}

