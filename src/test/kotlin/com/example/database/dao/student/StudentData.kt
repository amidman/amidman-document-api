package com.example.database.dao.student

import com.example.database.dao.department.DepartmentData
import com.example.database.dao.group.GroupData
import com.example.database.dao.student_document.StudentDocumentData
import com.example.database.dao.user.UserData
import com.example.domain.model.student.StudentCreateModel
import com.example.domain.model.student.StudentDTO
import com.example.domain.model.student.StudentModel

object StudentData {


    const val EXIST_STUDENT_ID = 1

    val EXIST_STUDENT_CREATE_MODEL = StudentCreateModel(
        fio = UserData.EXIST_USER_FIO,
        document = StudentDocumentData.EXIST_STUDENT_DOCUMENT,
        groupName = GroupData.EXIST_GROUP.groupName
    )

    val EXIST_STUDENT_MODEL = StudentModel(
        id = EXIST_STUDENT_ID,
        user = UserData.EXIST_USER,
        document = StudentDocumentData.EXIST_STUDENT_DOCUMENT,
        group = GroupData.EXIST_GROUP,
        department = DepartmentData.EXIST_DEPARTMENT,
    )

    val EXIST_STUDENT = StudentDTO(
        id = EXIST_STUDENT_ID,
        userId = UserData.EXIST_USER_ID,
        groupId = GroupData.EXIST_GROUP_ID,
        studentDocumentId = StudentDocumentData.EXIST_STUDENT_DOCUMENT_ID,
    )
}