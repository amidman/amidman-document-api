package com.example.database.dao.department

import com.example.domain.model.department.DepartmentDTO

object DepartmentData {
    const val EXIST_DEPARTMENT_ID = 1
    const val NO_EXIST_DEPARTMENT_ID = 2000
    val EXIST_DEPARTMENT =
        DepartmentDTO(id = EXIST_DEPARTMENT_ID, departmentName = "Pete", departmentShortName = "Kaycie")
    val NO_EXIST_DEPARTMENT =
        DepartmentDTO(id = NO_EXIST_DEPARTMENT_ID, departmentName = "Russ", departmentShortName = "Rod")

    val departmentList = mutableListOf(EXIST_DEPARTMENT)
}