val ktorVersion: String by project
val kotlinVersion: String by project
val logbackVersion: String by project
val exposedVersion: String by project
val postgresConnectorVersion: String by project
val koinVersion:String by project
val flywayVersion:String by project
val asposeVersion:String by project
val swaggerVersion:String by project
val dateTimeVersion:String by project
val passwordEncryptVersion:String by project
val junit5Version:String by project
val mockkVersion:String by project
val mockitoVersion:String by project
val apacheValidationVersion:String by project

plugins {
    application
    kotlin("jvm") version "1.7.22"
    id("io.ktor.plugin") version "2.1.3"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.7.22"
}

group = "com.example"
version = "0.0.1"
application {
    mainClass.set("io.ktor.server.cio.EngineMain")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    //for swagger ui
    maven("https://jitpack.io")
    //for file changer
    maven(uri("https://repository.aspose.com/repo/"))
    mavenCentral()
}

dependencies {

    //content negotiation
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktorVersion")
    //ktor server core
    implementation("io.ktor:ktor-server-core-jvm:$ktorVersion")
    //ktor serialization
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktorVersion")
    //ktor server common host
    implementation("io.ktor:ktor-server-host-common-jvm:$ktorVersion")
    //ktor status pages
    implementation("io.ktor:ktor-server-status-pages-jvm:$ktorVersion")
    //ktor auth
    implementation("io.ktor:ktor-server-auth-jvm:$ktorVersion")
    //ktor auth jwt
    implementation("io.ktor:ktor-server-auth-jwt-jvm:$ktorVersion")
    //ktor cio engine
    implementation("io.ktor:ktor-server-netty-jvm:$ktorVersion")
    //logback
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    //ktor test implementations
    testImplementation("io.ktor:ktor-server-tests-jvm:$ktorVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit5Version")
    testImplementation("io.mockk:mockk:$mockkVersion")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$mockitoVersion")
    testImplementation(kotlin("test"))
    //ktor request validation
    implementation("io.ktor:ktor-server-request-validation:$ktorVersion")
    //ktor koin
    implementation("io.insert-koin:koin-ktor:$koinVersion")
    //Password Encryption
    implementation("at.favre.lib:bcrypt:$passwordEncryptVersion")
    //exposed
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposedVersion")
    //postgres connector
    implementation("org.postgresql:postgresql:$postgresConnectorVersion")
    //kotlinx localdatetime
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:$dateTimeVersion")
    //swagger ui
    implementation("io.github.smiley4:ktor-swagger-ui:$swaggerVersion")
    //file changer
    implementation("com.aspose:aspose-pdf:$asposeVersion")
    //flyway
    implementation("org.flywaydb:flyway-core:$flywayVersion")
    //apache validation
    implementation("commons-validator:commons-validator:$apacheValidationVersion")
}

tasks.test{
    useJUnitPlatform()
}
tasks.shadowJar {
    setProperty("zip64",true)
}